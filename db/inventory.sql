-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2019 at 08:53 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `no_inventaris` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jumlah_sisa` int(11) NOT NULL,
  `tanggal_perolehan` date NOT NULL,
  `jenis` int(11) NOT NULL,
  `sumber_dana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `no_inventaris`, `nama_barang`, `jumlah`, `jumlah_sisa`, `tanggal_perolehan`, `jenis`, `sumber_dana`) VALUES
(1, 11111111, 'Kipas angin semilir', 2, 2, '2019-08-20', 3, 2),
(2, 11111112, 'Kursi Komputer X', 12, 12, '2019-08-20', 1, 2),
(4, 11111113, 'AC Panasonic', 15, 10, '2019-09-05', 2, 1),
(5, 1222222222, 'AC super', 10, 10, '2019-09-06', 2, 2),
(6, 123456, 'Komputer', 20, 10, '2019-09-06', 3, 2),
(7, 12345678, 'Proyektor', 15, 10, '2019-09-07', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_barang`
--

CREATE TABLE `detail_barang` (
  `id` int(11) NOT NULL,
  `tanggal_cek` date NOT NULL,
  `ruang` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah_barang_di_ruang` int(11) NOT NULL,
  `kondisi_sedang` int(11) NOT NULL,
  `kondisi_rusak` int(11) NOT NULL,
  `keterangan` text,
  `kondisi_baik` int(11) NOT NULL,
  `pencatat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_barang`
--

INSERT INTO `detail_barang` (`id`, `tanggal_cek`, `ruang`, `barang`, `jumlah_barang_di_ruang`, `kondisi_sedang`, `kondisi_rusak`, `keterangan`, `kondisi_baik`, `pencatat`) VALUES
(3, '2019-09-07', 5, 6, 6, 0, 0, NULL, 8, 1),
(4, '2019-09-09', 3, 6, 4, 0, 0, NULL, 2, 1),
(5, '2019-09-10', 6, 4, 2, 0, 0, NULL, 2, 1),
(8, '2019-09-10', 5, 4, 2, 0, 0, NULL, 2, 1),
(9, '2019-09-11', 2, 4, 1, 0, 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(11) NOT NULL,
  `no_inventaris` int(11) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `no_inventaris`, `jenis`, `kategori`) VALUES
(1, 11111111, 'jenis a', 3),
(2, 11111112, 'jenis b', 4),
(3, 2147483647, 'res', 5);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
(3, 'Alat Tulis Kantor'),
(4, 'Prasarana'),
(5, 'Sarana');

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` int(11) NOT NULL,
  `detail_barang` int(11) NOT NULL,
  `tanggal_maintenance` date NOT NULL,
  `uraian_maintenance` text NOT NULL,
  `next_maintenance` date NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penempatan`
--

CREATE TABLE `penempatan` (
  `id` int(11) NOT NULL,
  `tanggal_penempatan` date NOT NULL,
  `penanggung_jawab` int(11) DEFAULT NULL,
  `ruang_asal` int(11) DEFAULT NULL,
  `ruang_tujuan` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penempatan`
--

INSERT INTO `penempatan` (`id`, `tanggal_penempatan`, `penanggung_jawab`, `ruang_asal`, `ruang_tujuan`, `barang`, `jumlah`, `user`) VALUES
(10, '2019-09-07', 1, 1, 5, 6, 10, 1),
(11, '2019-09-11', 1, 5, 3, 6, 2, 1),
(13, '2019-09-10', 2, 5, 3, 6, 2, 1),
(14, '2019-09-09', 1, 1, 3, 7, 5, 1),
(15, '2019-09-10', 2, 1, 6, 4, 5, 1),
(18, '2019-09-10', 2, 6, 5, 4, 3, 1),
(19, '2019-09-11', 1, 5, 2, 4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `id` int(11) NOT NULL,
  `ruangan` varchar(255) NOT NULL,
  `penanggung_jawab_ruangan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ruangan`
--

INSERT INTO `ruangan` (`id`, `ruangan`, `penanggung_jawab_ruangan`) VALUES
(1, 'Gudang Persediaan', NULL),
(2, 'X MIA 1', NULL),
(3, 'X MIA 2', NULL),
(4, 'X MIA 3', NULL),
(5, 'X MIA 4', NULL),
(6, 'X MIA 5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `tahun_periode` int(11) NOT NULL,
  `nip` bigint(20) NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `posisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `tahun_periode`, `nip`, `nama`, `posisi`) VALUES
(1, 2019, 6111640000055, 'Charisma Sri Puspito', 'Penanggung Jawab Inventaris'),
(2, 2016, 6111640000081, 'Andre Setiyawan Sumiharjo', 'Penanggung Jawab Lab Komputer');

-- --------------------------------------------------------

--
-- Table structure for table `sumber_dana`
--

CREATE TABLE `sumber_dana` (
  `id` int(11) NOT NULL,
  `sumber_dana` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sumber_dana`
--

INSERT INTO `sumber_dana` (`id`, `sumber_dana`) VALUES
(1, 'Bantuan Operasional Sekolah'),
(2, 'Sumbangan Alumni'),
(3, 'tes 1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jenis` (`jenis`),
  ADD KEY `sumber_dana` (`sumber_dana`);

--
-- Indexes for table `detail_barang`
--
ALTER TABLE `detail_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ruang` (`ruang`),
  ADD KEY `barang_2` (`barang`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori` (`kategori`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintenance`
--
ALTER TABLE `maintenance`
  ADD KEY `detail_barang` (`detail_barang`);

--
-- Indexes for table `penempatan`
--
ALTER TABLE `penempatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lokasi` (`ruang_tujuan`),
  ADD KEY `barang` (`barang`),
  ADD KEY `ruang_asal` (`ruang_asal`),
  ADD KEY `penanggung_jawab` (`penanggung_jawab`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penanggung_jawab_ruangan` (`penanggung_jawab_ruangan`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `sumber_dana`
--
ALTER TABLE `sumber_dana`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `detail_barang`
--
ALTER TABLE `detail_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `penempatan`
--
ALTER TABLE `penempatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sumber_dana`
--
ALTER TABLE `sumber_dana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`jenis`) REFERENCES `jenis` (`id`),
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`sumber_dana`) REFERENCES `sumber_dana` (`id`);

--
-- Constraints for table `detail_barang`
--
ALTER TABLE `detail_barang`
  ADD CONSTRAINT `detail_barang_ibfk_1` FOREIGN KEY (`ruang`) REFERENCES `ruangan` (`id`),
  ADD CONSTRAINT `detail_barang_ibfk_2` FOREIGN KEY (`barang`) REFERENCES `barang` (`id`);

--
-- Constraints for table `jenis`
--
ALTER TABLE `jenis`
  ADD CONSTRAINT `jenis_ibfk_1` FOREIGN KEY (`kategori`) REFERENCES `kategori` (`id`);

--
-- Constraints for table `maintenance`
--
ALTER TABLE `maintenance`
  ADD CONSTRAINT `maintenance_ibfk_1` FOREIGN KEY (`detail_barang`) REFERENCES `detail_barang` (`id`);

--
-- Constraints for table `penempatan`
--
ALTER TABLE `penempatan`
  ADD CONSTRAINT `penempatan_ibfk_1` FOREIGN KEY (`ruang_tujuan`) REFERENCES `ruangan` (`id`),
  ADD CONSTRAINT `penempatan_ibfk_2` FOREIGN KEY (`barang`) REFERENCES `barang` (`id`),
  ADD CONSTRAINT `penempatan_ibfk_3` FOREIGN KEY (`ruang_asal`) REFERENCES `ruangan` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
