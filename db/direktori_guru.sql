-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Sep 2019 pada 09.11
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `direktori_guru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen`
--

CREATE TABLE `dokumen` (
  `id` bigint(20) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text,
  `tipe_upload` int(11) NOT NULL,
  `short_file` varchar(32) NOT NULL,
  `lokasi_file` text NOT NULL,
  `kategori` int(11) DEFAULT NULL,
  `mata_pelajaran` int(11) DEFAULT NULL,
  `kelas` int(11) DEFAULT NULL,
  `total_download` bigint(20) NOT NULL DEFAULT '0',
  `tanggal_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dokumen`
--

INSERT INTO `dokumen` (`id`, `judul`, `deskripsi`, `tipe_upload`, `short_file`, `lokasi_file`, `kategori`, `mata_pelajaran`, `kelas`, `total_download`, `tanggal_upload`, `username`) VALUES
(15, 'Silabus Bahasa Indonesia', '', 1, '522f3758aaa7259d1cf63bd116977284', 'assets/upload/direktori_guru/berkas/1566824253_Manual_Gitlab.pdf', 2, 2, 3, 3, '2019-08-26 00:00:00', '1111101'),
(16, 'qwerty', 'Silahkan lihat video ini!', 2, '60483cb77e59321a72c06db376775af1', 'https://www.youtube.com/watch?v=5bCT1arSHo0', 2, 2, 3, 0, '2019-09-30 01:43:36', 'tono'),
(20, 'Jadwal Les', '', 1, 'd9f0ce51c1f22ed42d494167b0b985b1', 'assets/upload/direktori_guru/berkas/1567548371_1111111.JPG', 19, 2, 3, 0, '2019-09-04 05:06:11', '1111101'),
(23, 'Laporan Kehilangan', '', 2, 'b42049f6b47da464ee5c471783f3578d', 'www.google.com', 2, 1, 3, 0, '2019-09-12 14:48:23', 'admin');

--
-- Trigger `dokumen`
--
DELIMITER $$
CREATE TRIGGER `triggerDownloadLog` AFTER UPDATE ON `dokumen` FOR EACH ROW IF NEW.total_download > OLD.total_download THEN
INSERT INTO `download`(`dokumen`) VALUES (OLD.id);
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `download`
--

CREATE TABLE `download` (
  `id` bigint(20) NOT NULL,
  `tgl_download` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dokumen` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `download`
--

INSERT INTO `download` (`id`, `tgl_download`, `dokumen`) VALUES
(20, '2019-09-04 04:18:18', 15),
(21, '2019-09-04 04:18:23', 15),
(22, '2019-09-05 05:29:12', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`, `keterangan`) VALUES
(2, 'Umum', 'video pembelajaran'),
(3, 'latihan soal', 'latihan soal pelajaran'),
(5, 'audio bahasa inggris', 'latihan listening'),
(6, 'audio bahasa daerah', 'listening bahasa daerah'),
(8, 'petunjuk penggunaan alat lab', 'pdf petunjuk alat-alat lab'),
(9, 'tutorial komputer', 'panduan komputer pelajaran TIK'),
(10, 'hasta karya', 'panduan hasta karya'),
(12, 'silabus', 'silabus pembelajaran'),
(19, 'resep masakan', 'resep masakan nusantara'),
(20, 'tips trik', 'buku tips mengajar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id`, `kelas`, `keterangan`) VALUES
(3, 'Umum', ''),
(4, 'Kelas X IPA 1', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` int(11) NOT NULL,
  `mata_pelajaran` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `mata_pelajaran`, `keterangan`) VALUES
(1, 'Umum', ''),
(2, 'Bahasa Indonesia', ''),
(3, 'Bahasa Inggris', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_upload`
--

CREATE TABLE `tipe_upload` (
  `id` int(11) NOT NULL,
  `tipe_upload` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe_upload`
--

INSERT INTO `tipe_upload` (`id`, `tipe_upload`) VALUES
(1, 'Berkas'),
(2, 'Link');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipe_upload` (`tipe_upload`),
  ADD KEY `kategori` (`kategori`),
  ADD KEY `mata_pelajaran` (`mata_pelajaran`),
  ADD KEY `kelas` (`kelas`);

--
-- Indeks untuk tabel `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `download_ibfk_1` (`dokumen`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tipe_upload`
--
ALTER TABLE `tipe_upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `download`
--
ALTER TABLE `download`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tipe_upload`
--
ALTER TABLE `tipe_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dokumen`
--
ALTER TABLE `dokumen`
  ADD CONSTRAINT `dokumen_ibfk_1` FOREIGN KEY (`tipe_upload`) REFERENCES `tipe_upload` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `dokumen_ibfk_2` FOREIGN KEY (`kategori`) REFERENCES `kategori` (`id`),
  ADD CONSTRAINT `dokumen_ibfk_3` FOREIGN KEY (`mata_pelajaran`) REFERENCES `mata_pelajaran` (`id`),
  ADD CONSTRAINT `dokumen_ibfk_4` FOREIGN KEY (`kelas`) REFERENCES `kelas` (`id`);

--
-- Ketidakleluasaan untuk tabel `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `download_ibfk_1` FOREIGN KEY (`dokumen`) REFERENCES `dokumen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
