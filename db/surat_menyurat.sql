-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2019 at 11:47 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat_menyurat`
--

-- --------------------------------------------------------

--
-- Table structure for table `detil_surat`
--

CREATE TABLE `detil_surat` (
  `id_detil` int(20) NOT NULL,
  `no_agenda` varchar(100) NOT NULL,
  `tujuan_disposisi` varchar(100) NOT NULL,
  `user_disposisi` varchar(100) NOT NULL,
  `pemroses` varchar(100) NOT NULL,
  `tgl_proses` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  `sifat` varchar(100) NOT NULL,
  `isi_disposisi` varchar(250) NOT NULL,
  `riwayat` varchar(100) NOT NULL,
  `selesai` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `id_surat` int(11) NOT NULL,
  `no_agenda` varchar(100) NOT NULL,
  `no_surat` varchar(100) CHARACTER SET latin1 NOT NULL,
  `asal_surat` varchar(250) CHARACTER SET latin1 NOT NULL,
  `perihal_surat` mediumtext CHARACTER SET latin1 NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_terima` datetime NOT NULL,
  `berkas_surat` text CHARACTER SET latin1 NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `status_surat` int(11) NOT NULL,
  `status_tu` int(1) NOT NULL,
  `status_staf` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detil_surat`
--
ALTER TABLE `detil_surat`
  ADD PRIMARY KEY (`id_detil`),
  ADD KEY `no_agenda` (`no_agenda`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `no_agenda` (`no_agenda`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detil_surat`
--
ALTER TABLE `detil_surat`
  MODIFY `id_detil` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
