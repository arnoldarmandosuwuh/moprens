-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2019 at 04:32 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `presensi_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `izin_siswa`
--

CREATE TABLE `izin_siswa` (
  `id` bigint(20) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `tanggal_awal` datetime NOT NULL,
  `tanggal_akhir` datetime NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `izin_siswa`
--

INSERT INTO `izin_siswa` (`id`, `nis`, `tanggal_awal`, `tanggal_akhir`, `keterangan`) VALUES
(2, '123', '2019-08-22 00:00:00', '2019-08-23 00:00:00', 'Izin');

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

CREATE TABLE `presensi` (
  `id` bigint(20) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `check_time` datetime NOT NULL,
  `check_type` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `presensi`
--

INSERT INTO `presensi` (`id`, `nis`, `check_time`, `check_type`) VALUES
(49, '123', '2019-08-23 18:23:49', 'I'),
(50, '123', '2019-08-23 18:03:55', 'I'),
(51, '124', '2019-08-23 18:29:06', 'I'),
(52, '124', '2019-08-23 18:28:06', 'I'),
(53, '124', '2019-08-23 18:03:10', 'I'),
(54, '124', '2019-08-23 17:59:52', 'I'),
(55, '126', '2019-08-23 18:34:03', 'I'),
(56, '126', '2019-08-23 18:33:08', 'I'),
(57, '126', '2019-08-23 18:32:52', 'I'),
(58, '890', '2019-08-23 15:55:59', 'O'),
(59, '890', '2019-08-23 15:51:32', 'I'),
(60, '891', '2019-08-23 15:59:05', 'I'),
(61, '892', '2019-08-23 15:55:59', 'O'),
(62, '892', '2019-08-23 15:51:32', 'I'),
(63, '893', '2019-08-23 18:29:00', 'I'),
(64, '893', '2019-08-23 15:59:05', 'I');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `izin_siswa`
--
ALTER TABLE `izin_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `izin_siswa`
--
ALTER TABLE `izin_siswa`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `presensi`
--
ALTER TABLE `presensi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
