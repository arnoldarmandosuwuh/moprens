<?php echo show_alert($this->session->flashdata()); ?>

<div class="row">
    <div class="col-md-3">
        <a href="<?php echo base_url('siswa/read_csv'); ?>" class="dashboard-link">
            <div class="card bg-white text-blue">
            <!-- <div class="card bg-danger text-white"> -->
                <div class="card-body text-center">
                    <h1><i class="nav-icon icon-cloud-download"></i></h1>
                    <h3>Impor Data Siswa</h3>
                </div>
            </div>
        </a>
    </div>
    
    <div class="col-md-3">
        <a href="<?php echo base_url('presensi_online/master/ambil/') ?>" class="dashboard-link">
            <div class="card bg-white text-blue">
            <!-- <div class="card bg-warning text-white"> -->
                <div class="card-body text-center">
                    <h1><i class="nav-icon icon-cloud-upload"></i></h1>
                    <h3>Proses Data Finger</h3>
                </div>
            </div>
        </a>
    </div>

</div>
