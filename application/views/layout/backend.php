<!DOCTYPE html>
<html lang="id">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo show($title) .' | ' . $this->config->item('app_title'); ?></title>
    <?php foreach ($list_css as $key_css => $url_css) : ?>
    <link href="<?php echo $url_css; ?>" rel="stylesheet">
    <?php endforeach; ?>
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar <?php echo $modul_custom['bg_color']; ?>">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="fa fa-bars" style="color: <?php echo $modul_custom['icon_color'] ?>;"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>">
        <img class="navbar-brand-full" src="<?php echo base_url('assets/img/brand/'.$modul_custom['icon_wide']); ?>" width="130" height="35" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="<?php echo base_url('assets/img/brand/'.$modul_custom['icon_square']); ?>" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="fa fa-bars" style="color: <?php echo $modul_custom['icon_color'] ?>;"></span>
      </button>
      
      <ul class="nav navbar-nav ml-auto">
        <!--
        <li class="nav-item dropdown d-md-down-none">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-envelope" style="color: <?php echo $modul_custom['icon_color']; ?>"></i>
            <span class="badge badge-pill badge-info">7</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
            <div class="dropdown-header text-center">
              <strong>You have 4 messages</strong>
            </div>
            <a class="dropdown-item" href="#">
              <div class="message">
                <div class="py-3 mr-3 float-left">
                  <div class="avatar">
                    <img class="img-avatar" src="<?php echo base_url('assets/img/avatars/7.jpg'); ?>" alt="admin@bootstrapmaster.com">
                    <span class="avatar-status badge-success"></span>
                  </div>
                </div>
                <div>
                  <small class="text-muted">John Doe</small>
                    <small class="text-muted float-right mt-1">5 minutes ago</small>
                </div>
                <div class="text-truncate font-weight-bold">
                  Important message
                </div>
                <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
                </div>
              </div>
            </a>
            <a class="dropdown-item text-center" href="#">
              <strong>View all messages</strong>
            </a>
          </div>
        </li> -->

        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            
            <strong><?php show($this->ion_auth->user()->row()->first_name); ?></strong>
            <img class="img-avatar" src="<?php echo base_url('assets/img/avatars/user.jpg'); ?>" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>User Login</strong>
            </div>
            <a class="dropdown-item" href="<?php echo base_url('auth/change-password'); ?>">
              <i class="fa fa-lock"></i> Ganti Password
            </a>
            
            <?php if (current_url() != base_url()) : ?>
            <a class="dropdown-item" href="<?php echo base_url(); ?>">
              <i class="fa fa-arrow-left"></i> Kembali ke Sistersmazaba
            </a>
            <?php endif; ?>
            
            <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            
            <li class="nav-title">Menu Utama</li>
            <?php if (is_array($dashboard_menu)) : ?>
            <?php foreach ($dashboard_menu as $title => $menu) : ?>
            
                <li class="nav-item nav-dropdown <?php echo !empty($menu['submenu']) ? expand_menu($menu['submenu']) : ''; ?>">
                  
                  <a class="nav-link <?php echo !empty($menu['submenu']) ? 'nav-dropdown-toggle': '' ; ?>" href="<?php echo !empty($menu['url']) ? $menu['url'] : '#' ; ?>">
                    <i class="<?php echo $menu['icon']; ?>"></i> <?php echo $title; ?>
                  </a>

                  <?php if (!empty($menu['submenu'])) : ?>

                      <ul class="nav-dropdown-items">
                        
                        <?php foreach ($menu['submenu'] as $submenu_title => $submenu) : ?>
                            <li class="nav-item">
                              <a class="nav-link <?php echo !empty($submenu['url']) ? active_menu($submenu['url']) : '' ; ?>" href="<?php echo $submenu['url']; ?>">
                                <i class="<?php echo $submenu['icon']; ?>"></i> <?php echo $submenu_title; ?>
                              </a>
                            </li>
                        <?php endforeach; ?>

                      </ul>

                <?php endif; ?>

                </li>
            
            <?php endforeach; ?>
          <?php endif; ?>
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
        <?php
          if (isset($breadcrumbs)) :
            foreach ($breadcrumbs as $key_bread => $breadcrumb) :
        ?>

          <?php
            end($breadcrumbs);
            if ($key_bread == key($breadcrumbs)) :
          ?>
            <li class="breadcrumb-item">
              <?php echo $breadcrumb['title'] ?>
            </li>
        <?php else: ?>
            <li class="breadcrumb-item">
              <a href="<?php echo $breadcrumb['url']; ?>"><?php echo $breadcrumb['title'] ?></a>
            </li>
          <?php
              endif;
            endforeach;
          endif;
        ?>
          &nbsp;
          <!-- Breadcrumb Menu-->
        </ol>
        <div class="container">
          <div class="animated fadeIn">
            <?php echo $content; ?>
          </div>
        </div>
      </main>
      <aside class="aside-menu">
        <!-- Tab panes-->
      </aside>
    </div>
    <footer class="app-footer">
      <div>
        <span>&copy; 2019 <a href="https://sman1babadanponorogo.sch.id">SMAZABA</a></span>
      </div>
      <div class="ml-auto">
        <span>Supported by</span>
        <a href="https://www.its.ac.id">ITS</a>
      </div>
    </footer>
    
    <script type="text/javascript">
      var base_url = '<?php echo base_url(); ?>';
    </script>

    <!-- generate js yang akan di load di halaman backend -->
    <?php foreach ($list_js as $key_js => $url_js) : ?>
    <script src="<?php echo $url_js; ?>"></script>
    <?php endforeach; ?>

  </body>
</html>
