<!DOCTYPE html>
<html lang="id">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo $title .' | ' . $this->config->item('app_title'); ?></title>
    <!-- Icons-->
    <link href="<?php echo base_url('assets/vendors/@coreui/icons/css/coreui-icons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/flag-icon-css/css/flag-icon.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/simple-line-icons/css/simple-line-icons.css'); ?>" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/pace-progress/css/pace.min.css'); ?>" rel="stylesheet">
  </head>
  <body class="app flex-row align-items-center" style="background-image: url(<?php echo base_url('assets/img/login-background.jpg'); ?>); background-size: cover;">
    <div class="container">
      <?php echo $content; ?>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="<?php echo base_url('assets/vendors/jquery/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/popper.js/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/pace-progress/js/pace.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/@coreui/coreui/js/coreui.min.js'); ?>"></script>
  </body>
</html>
