<div class="row">
  <div class="col-md-12">
    <div class="card">
      <?php echo form_open('permission/add-permission', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-lock"></i> Tambah Hak Akses
      </div>
      <div class="card-body">
        <div class="form-group row">
          <?php echo form_label('Key Hak Akses', 'perm_key', array('class' => 'control-label col-md-2')); ?>
          <div class="col-md-10">
            <?php echo form_input($perm_key); ?>
            <small id="perm_key_help" class="form-text text-muted">Jangan gunakan spasi, gunakan format seperti contoh : direktori_guru>master>guru>read</small>
            <?php echo form_error('perm_key', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        
        <div class="form-group row">
          <?php echo form_label('Nama Hak Akses', 'perm_name', array('class' => 'control-label col-md-2')); ?>
          <div class="col-md-10">
            <?php echo form_input($perm_name); ?>
            <small id="perm_name_help" class="form-text text-muted">contoh : Create Barang Datang</small>
            <?php echo form_error('perm_name', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('permission'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
