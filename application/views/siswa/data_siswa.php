<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Siswa
        <div class="pull-right">
          <!-- <a href="<?php echo base_url('siswa/sinkron_lokal'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Sinkronisasi DB Lokal</a> -->
          <!-- <a href="<?php echo base_url('siswa/sinkronisasi'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Ekspor Fingerprint</a> -->
          <a href="<?php echo base_url('siswa/sinkron_anggota'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Sinkronisasi Anggota</a>
          <a href="<?php echo base_url('siswa/sinkron_user'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Sinkronisasi User</a>
          <a href="<?php echo base_url('siswa/get_csv'); ?>" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Ekspor CSV</a>
          <!-- <a href="<?php echo base_url('siswa/read_csv'); ?>" class="btn btn-success btn-sm"><i class="fa fa-upload"></i> Impor CSV</a> -->
          <a href="<?php echo base_url('siswa/tambah'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed siswa-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>NIS</th>
                      <th>Nama</th>
                      <th>Tempat</th>
                      <th>Tanggal Lahir</th>
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                    <!-- konten di-load secara ajax -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
