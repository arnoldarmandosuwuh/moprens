<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open_multipart('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Tambah Siswa
      </div>
      <div class="card-body">
      <div class="form-group row">
          <div class="col-12 foto align-center">
            <?php if(!empty($form['foto']['input']['value'])){ ?>
              <img src="<?php echo base_url().$form['foto']['input']['value'] ?>">
            <?php } else { ?>
              <img src="<?php echo base_url('/assets/img/avatars/user_default.png') ?>">
            <?php } ?>
          </div>
        </div>
        <div class="form-group row">
        <?php echo form_label($form['kode']['label'],'kode', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['kode']['input']); ?>
            <?php echo form_error('kode', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        
        <div class="form-group row">
        <?php echo form_label($form['nama']['label'],'nama', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['nama']['input']); ?>
            <?php echo form_error('nama', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
        <?php echo form_label($form['jenis_kelamin']['label'],'jenis_kelamin', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_dropdown($form['jenis_kelamin']['input']); ?>
            <?php echo form_error('jenis_kelamin', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        
        <div class="form-group row">
        <?php echo form_label($form['tempat_lahir']['label'],'tempat_lahir', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tempat_lahir']['input']); ?>
            <?php echo form_error('tempat_lahir', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

      <div class="form-group row">
        <?php echo form_label($form['tanggal_lahir']['label'],'tanggal_lahir', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tanggal_lahir']['input']); ?>
            <?php echo form_error('tanggal_lahir', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

      <div class="form-group row">
        <?php echo form_label($form['alamat']['label'],'alamat', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['alamat']['input']); ?>
            <?php echo form_error('alamat', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

      <div class="form-group row">
        <?php echo form_label($form['foto']['label'],'foto', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['foto']['input']); ?>
            <?php echo form_error('foto', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

        <div class="form-group row">
        <?php echo form_label($form['keterangan']['label'],'keterangan', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['keterangan']['input']); ?>
            <?php echo form_error('keterangan', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>        
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('siswa'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>

<style type="text/css">
.foto {
  text-align: center;
  display: inline-block;
}

.foto img {
  height: 300px;
  width: 200px;
  display: inline-block;
}
</style>