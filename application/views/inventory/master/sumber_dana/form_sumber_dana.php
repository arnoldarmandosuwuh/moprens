<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-wallet"></i> Tambah Sumber Dana
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['sumber_dana']['label'], 'sumber_dana', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['sumber_dana']['input']); ?>
                        <?php echo form_error('sumber_dana', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/master/sumber_dana'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>