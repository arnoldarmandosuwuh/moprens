<div class="row">
    <div class="col-md-12">

        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-screen-desktop"></i> Data Inventaris
                <?php if ($this->ion_auth_acl->has_permission('inventory>master>barang>create')) : ?>
                    <div class="pull-right">
                        <a href="<?php echo base_url('inventory/master/barang/tambah'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed barang-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Nomor Inventaris</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Awal</th>
                                        <th>Jumlah Sisa</th>
                                        <th>Jenis</th>
                                        <th>Tanggal Perolehan</th>
                                        <th>Sumber Dana</th>
                                        <th style="width: 10px;">Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- konten di-load secara ajax -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>