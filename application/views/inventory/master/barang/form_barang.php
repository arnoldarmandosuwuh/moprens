<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-screen-desktop"></i> Tambah Barang
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['no_inventaris']['label'], 'no_inventaris', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['no_inventaris']['input']); ?>
                        <?php echo form_error('no_inventaris', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['nama_barang']['label'], 'nama_barang', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['nama_barang']['input']); ?>
                        <?php echo form_error('nama_barang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['jumlah']['label'], 'jumlah', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['jumlah']['input']); ?>
                        <?php echo form_error('jumlah', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['tanggal_perolehan']['label'], 'tanggal_perolehan', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['tanggal_perolehan']['input']); ?>
                        <?php echo form_error('tanggal_perolehan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['jenis']['label'], 'jenis', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="jenis" id="jenis">
                            <option value="">-- Pilih Jenis --</option>
                            <?php foreach ($form['jenis']['option'] as $item) { ?>
                                <?php if ($form['jenis']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->jenis; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->jenis; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('jenis', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['sumber_dana']['label'], 'sumber_dana', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="sumber_dana" id="sumber_dana">
                            <option value="">-- Pilih Sumber Dana --</option>
                            <?php foreach ($form['sumber_dana']['option'] as $item) { ?>
                                <?php if ($form['sumber_dana']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->sumber_dana; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->sumber_dana; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('sumber_dana', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/master/barang'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>