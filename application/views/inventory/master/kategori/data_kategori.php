<div class="row">
    <div class="col-md-12">

        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-flag"></i> Kategori
                <?php if ($this->ion_auth_acl->has_permission('inventory>master>kategori>create')) : ?>
                    <div class="pull-right">
                        <a href="<?php echo base_url('inventory/master/kategori/tambah'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed kategori-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Kategori</th>
                                        <th style="width: 10px;">Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- konten di-load secara ajax -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>