<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-screen-desktop"></i> Tambah Pegawai
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['nip']['label'], 'nip', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['nip']['input']); ?>
                        <?php echo form_error('nip', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['nama']['label'], 'nama', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['nama']['input']); ?>
                        <?php echo form_error('nama', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['posisi']['label'], 'posisi', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['posisi']['input']); ?>
                        <?php echo form_error('posisi', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['tahun_periode']['label'], 'tahun_periode', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['tahun_periode']['input']); ?>
                        <?php echo form_error('tahun_periode', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/master/pegawai'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>