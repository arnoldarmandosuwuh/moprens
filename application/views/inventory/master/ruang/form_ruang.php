<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-info"></i> Tambah Ruangan
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['ruangan']['label'], 'ruangan', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['ruangan']['input']); ?>
                        <?php echo form_error('ruangan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['penanggung_jawab_ruangan']['label'], 'penanggung_jawab_ruangan', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="penanggung_jawab_ruangan" id="penanggung_jawab_ruangan">
                            <option value="">-- Penanggung Jawab Ruangan --</option>
                            <?php foreach ($form['penanggung_jawab_ruangan']['option'] as $item) { ?>
                                <?php if ($form['penanggung_jawab_ruangan']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->penanggung_jawab_ruangan; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->penanggung_jawab_ruangan; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('penanggung_jawab_ruangan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/master/ruang'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>