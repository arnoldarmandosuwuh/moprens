<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-flag"></i> Tambah Jenis
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['no_inventaris']['label'], 'no_inventaris', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['no_inventaris']['input']); ?>
                        <?php echo form_error('no_inventaris', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['jenis']['label'], 'jenis', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['jenis']['input']); ?>
                        <?php echo form_error('jenis', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['kategori']['label'], 'kategori', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="kategori" id="kategori">
                            <option value="">-- Pilih Kategori --</option>
                            <?php foreach ($form['kategori']['option'] as $item) { ?>
                                <?php if ($form['kategori']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->kategori; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->kategori; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kategori', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/master/jenis'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
</div>