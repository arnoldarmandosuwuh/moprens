<div class="row">
    <div class="col-md-12">
        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-magic-wand"></i> Filter Data
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label"> <?php echo form_label('Jenis Filter :', 'jenis_filter'); ?></label>
                    <div class="col-md-4">
                        <select class="form-control app-select2" name="jenis_filter" id="jenis_filter">
                            <option value="1">Ruangan</option>
                            <option value="2">Inventaris</option>
                        </select>
                    </div>
                    <label class="col-md-1 col-form-label filter_label" id="filter_label">Filter :</label>
                    <div class="col-md-5">
                        <!-- inisialisasi pake ruangan id ke 1 dulu -->
                        <select class="form-control app-select2" name="filter" id="filter">

                        </select>
                        <!-- load dengan ajax berdasarkan ruang atau barang -->
                    </div>
                </div>
                <div class="pull-right mb-2">
                    <?php echo form_button(array('content' => 'Proses', 'id' => 'filter_button', 'class' => 'filter_button btn btn-primary')); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-doc"></i> Laporan Penempatan Inventaris
                <div class="pull-right">
                    <a href="<?php echo base_url('inventory/report/rpt_inventaris/print'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> Cetak</a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <div id="notice"></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed inventaris-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Nama Barang</th>
                                        <th>Type/Jenis</th>
                                        <th>No. Inventaris</th>
                                        <th style="width: 12px;">Tahun</th>
                                        <th style="width: 10px;">Jumlah</th>
                                        <th style="width: 10px;">Baik</th>
                                        <th>Rusak Ringan</th>
                                        <th>Rusak Berat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- konten di-load secara ajax -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>