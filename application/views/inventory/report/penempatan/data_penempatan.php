<div class="row">
    <div class="col-md-12">
        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get')); ?>
            <div class="card-header">
                <i class="nav-icon icon-calendar"></i> Set Filter Tanggal
            </div>
            <div class="card-body">
                <div class="form-group row input-daterange">
                    <label class="col-md-2 col-form-label">Tanggal Awal : </label>
                    <div class="col-md-4">
                        <input type="text" name="tgl_awal" class="form-control app-datepicker" value="<?php echo $tgl_awal ?>">
                        <?php echo form_error('tanggal_awal', '<label class="text-danger">', '</label>'); ?>
                    </div>
                    <label class="col-md-2 col-form-label">Tanggal Akhir : </label>
                    <div class="col-md-4">
                        <input type="text" name="tgl_akhir" class="form-control app-datepicker" value="<?php echo $tgl_akhir ?>">
                        <?php echo form_error('tanggal_akhir', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="pull-right mb-2">
                    <?php echo form_button(array('content' => 'Proses', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-doc"></i> Laporan Penempatan Inventaris
                <?php if ($this->ion_auth_acl->has_permission('inventory>report>rpt_penempatan>print')) : ?>
                    <div class="pull-right">
                        <a href="<?php echo base_url('inventory/report/rpt_penempatan/print?tgl_awal=' . $tgl_awal . '&tgl_akhir=' . $tgl_akhir); ?>" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> Cetak</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <div class="text-info notice" id="notice">
                                <h5>Data penempatan dari tanggal <span class="font-weight-bold font-italic"><?php echo output_date($tgl_awal); ?></span> sampai tanggal <span class="font-italic font-weight-bold"><?php echo output_date($tgl_akhir); ?></span></h5>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed penempatan-datatable" id="penempatan-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Tanggal</th>
                                        <th>Penerima</th>
                                        <th>Ruang Asal</th>
                                        <th>Ruang Tujuan</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Pencatat</th>
                                        <th style="width: 10px;">Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1;
                                    foreach ($data_print as $key1) : ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no++; ?></td>
                                            <?php foreach ($key1 as $key => $value) : ?>
                                                <?php if ($key == 'tanggal_penempatan') : ?>
                                                    <td><?php echo output_date($value); ?></td>
                                                <?php elseif ($key == 'penanggung_jawab') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'ruang_asal') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'ruang_tujuan') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'barang') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'jumlah') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'user') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'id') : ?>
                                                    <td><a class="btn btn-primary btn-sm" target="_blank" href="<?php echo base_url('inventory/report/rpt_penempatan/cetak/' . $value); ?>"><i class="fa fa-print"></i> Cetak Detail</a></td>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>