<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo show($title) . ' | ' . $this->config->item('app_title'); ?></title>
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css'); ?>" rel="stylesheet">
    <style media="print">
        .dontprint {
            display: none;
        }

        @media print {
            @page {
                size: a4 potrait;
            }

            p,
            td {
                font-family: "Time New Roman", Times, Serif;
                font-size: 14pt;
            }

            h4,
            h3,
            h1 {
                font-family: "Time New Roman", Times, Serif;
            }

            thead th {
                vertical-align: middle !important;
                text-align: center;
            }
        }

        p,
        td {
            font-family: "Time New Roman", Times, Serif;
            font-size: 14pt;
        }

        h4,
        h3,
        h1 {
            font-family: "Time New Roman", Times, Serif;
        }

        thead th {
            vertical-align: middle !important;
            text-align: center;
        }
    </style>
</head>

<body class="bg-white" style="margins : minimum;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-3 center-img">
                        <img src="<?php echo base_url('assets/img/logo_sman1babadan.jpeg'); ?>" style="width:60%;" alt="">
                    </div>
                    <div class="col-md-7 text-center">
                        <h1 style="margin-top:3%;">SMAN 1 BABADAN PONOROGO</h1>
                        <h3 style="">Laporan Penempatan Inventaris</h3>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-12">
                        <hr style="border-width: 3px; display: block;">
                    </div>
                </div>

                <!-- KONTEN Laporan -->
                <div class="row mt-4 justify-content-center">
                    <div class="col-md-6 text-center">
                        <span>
                            <h4>Laporan Riwayat Penempatan Barang</h4>
                        </span>
                        <?php if ($tgl_awal && $tgl_akhir) : ?>
                            <h5>Tanggal : <?php echo output_date($tgl_awal); ?> - <?php echo output_date($tgl_akhir); ?></h5>
                        <?php else : ?>
                            <h6>Tanggal : Semua data Penempatan</h4>
                            <?php endif; ?>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed penempatan-datatable" id="penempatan-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Tanggal</th>
                                        <th>Penerima</th>
                                        <th>Ruang Asal</th>
                                        <th>Ruang Tujuan</th>
                                        <th>Nama Barang</th>
                                        <th style="width: 10px;">Jumlah</th>
                                        <th>Pencatat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1;
                                    foreach ($data_print as $key1) : ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no++; ?></td>
                                            <?php foreach ($key1 as $key => $value) : ?>
                                                <?php if ($key == 'tanggal_penempatan') : ?>
                                                    <td><?php echo output_date($value); ?></td>
                                                <?php elseif ($key == 'penanggung_jawab') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'ruang_asal') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'ruang_tujuan') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'barang') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php elseif ($key == 'jumlah') : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php else : ?>
                                                    <td><?php echo $value; ?></td>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-12 text-right pr-5">
                        <p class="">Ponorogo, <?php echo output_date(date('Y-m-d')); ?></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 text-center">
                        <p>Wakasek Sarana Prasana
                        </p>
                        <br><br><br><br>
                        <p><?php echo $waka_sarpras['nama']; ?></p>
                        <p><?php echo $waka_sarpras['nip']; ?></p>
                    </div>
                    <div class="col-md-6 text-center">
                        <p>Pencetak Laporan</p>
                        <br>
                        <br><br><br>
                        <p><?php echo $this->ion_auth->user()->row()->first_name; ?> <?php echo $this->ion_auth->user()->row()->last_name; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dontprint text-center" style="margin-top: 10px;">
        <a class="btn btn-default btn-print btn-warning" href="<?php echo base_url($link_kembali); ?>"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button class="btn btn-primary btn-print" onclick="window.print();"><i class="fa fa-print"></i> Cetak</button>
    </div>
</body>

</html>