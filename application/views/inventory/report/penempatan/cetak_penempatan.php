<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo show($title) . ' | ' . $this->config->item('app_title'); ?></title>
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css'); ?>" rel="stylesheet">
    <style media="print">
        @media print {
            @page {
                size: a4 potrait;
            }

            p,
            td {
                font-family: "Time New Roman", Times, Serif;
                font-size: 14pt;
            }

            h4,
            h3,
            h1 {
                font-family: "Time New Roman", Times, Serif;
            }
        }

        p,
        td {
            font-family: "Time New Roman", Times, Serif;
            font-size: 14pt;
        }

        h4,
        h3,
        h1 {
            font-family: "Time New Roman", Times, Serif;
        }
    </style>
</head>

<body class="bg-white" style="margins : minimum;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-3 center-img">
                        <img src="<?php echo base_url('assets/img/logo_sman1babadan.jpeg'); ?>" style="width:60%;" alt="">
                    </div>
                    <div class="col-md-7 text-center">
                        <h1 style="margin-top:3%;">SMAN 1 BABADAN PONOROGO</h1>
                        <h3 style="">Laporan Penempatan Inventaris</h3>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-12">
                        <hr style="border-width: 3px; display: block;">
                    </div>
                </div>

                <!-- KONTEN SURAT -->
                <div class="row mt-3 px-5">
                    <div class="col-md-12">
                        <p><span class="pl-5">Saya</span> yang bertanda tangan di bawah ini, menerangkan bahwa pada tanggal <?php echo output_date($data_cetak['tanggal_penempatan']); ?> telah dilakukan penempatan barang dari ruang <?php echo $data_cetak['ruang_asal']; ?> ke ruang <?php echo $data_cetak['ruang_tujuan']; ?> dengan rincian pemindahan sebagai berikut :</p>
                        <br>
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td scope="row" style="width:30%">Tanggal Penempatan</td>
                                            <td>: <?php echo output_date($data_cetak['tanggal_penempatan']); ?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Penanggung Jawab</td>
                                            <td>: <?php echo $data_cetak['penanggung_jawab']['nama']; ?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Ruang Asal</td>
                                            <td>: <?php echo $data_cetak['ruang_asal']; ?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Ruang Tujuan</td>
                                            <td>: <?php echo $data_cetak['ruang_tujuan']; ?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Nama Barang</td>
                                            <td>: <?php echo $data_cetak['barang']; ?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Jumlah dipindahkan</td>
                                            <td>: <?php echo $data_cetak['jumlah']; ?> buah.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <p><span class="pl-5">Demikian</span> surat ini saya buat dengan sadar tanpa ada paksaan dari pihak lain, untuk dapat dipergunakan sebagaimana mestinya. </p>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-12 text-right pr-5">
                        <p class="">Ponorogo, <?php echo output_date(date('Y-m-d')); ?></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 text-center">
                        <p>Penanggung Jawab Penempatan</p>
                        <br><br><br><br>
                        <p><?php echo $data_cetak['penanggung_jawab']['nama']; ?></p>
                        <p><?php echo $data_cetak['penanggung_jawab']['nip']; ?></p>
                    </div>
                    <div class="col-md-6 text-center">
                        <p>Pencatat Laporan</p>
                        <br>
                        <br><br><br>
                        <p><?php echo $data_cetak['user']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    window.print();
</script>

</html>