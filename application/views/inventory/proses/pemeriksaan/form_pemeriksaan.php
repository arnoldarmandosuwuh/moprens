<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-flag"></i> Proses Pemeriksaan Inventaris Ruang
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['tanggal_cek']['label'], 'tanggal_cek', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['tanggal_cek']['input']); ?>
                        <?php echo form_error('tanggal_cek', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['ruang']['label'], 'ruang', $form_label); ?>
                    <div class="col-md-10">
                        <select readonly="readonly" class="form-control" name="ruang" id="ruang">
                            <?php foreach ($form['ruang']['option'] as $item) { ?>
                                <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->ruangan; ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('ruang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['barang']['label'], 'barang', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="barang" id="barang">
                            <option value="">-- Pilih Barang --</option>
                            <?php foreach ($form['barang']['option'] as $item) { ?>
                                <?php if ($form['barang']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->nama_barang . ' ' . $item->tahun . ' | (' . $item->sisa . ') buah'; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->nama_barang . ' ' . $item->tahun . ' | (' . $item->sisa . ') buah'; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('barang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-10 offset-md-2">
                        <div id="lbl_jumlah_barang_di_ruang" class="pl-2 text-info font-weight-bold"></div>
                    </div>
                </div>
                <div class="form-group row input-daterange">
                    <label class="col-md-2 col-form-label">Kondisi Baik</label>
                    <div class="col-md-2">
                        <?php echo form_input($form['kondisi_baik']['input']); ?>
                        <?php echo form_error('kondisi_baik', '<label class="text-danger">', '</label>'); ?>
                    </div>

                    <label class="col-md-2 col-form-label">Rusak Ringan</label>
                    <div class="col-md-2">
                        <?php echo form_input($form['kondisi_sedang']['input']); ?>
                        <?php echo form_error('kondisi_sedang', '<label class="text-danger">', '</label>'); ?>
                    </div>

                    <label class="col-md-2 col-form-label">Rusak Berat</label>
                    <div class="col-md-2">
                        <?php echo form_input($form['kondisi_rusak']['input']); ?>
                        <?php echo form_error('kondisi_rusak', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <?php echo form_label($form['keterangan']['label'], 'keterangan', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_textarea($form['keterangan']['input']) ?>
                        <?php echo form_error('keterangan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/proses/penempatan'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>