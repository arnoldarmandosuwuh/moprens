<div class="row">
    <div class="col-md-12">

        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <div class="card-header">
                <i class="nav-icon icon-home"></i> Data Ruang Maintenance
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed ruang-periksa-datatable">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Nama Ruangan</th>
                                        <th>Terakhir Pemeriksaan</th>
                                        <th style="width: 25px;">Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- konten di-load secara ajax -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>