<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-flag"></i> Proses Maintenance Inventaris
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['tanggal_maintenance']['label'], 'tanggal_maintenance', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['tanggal_maintenance']['input']); ?>
                        <?php echo form_error('tanggal_maintenance', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['ruang']['label'], 'ruang', $form_label); ?>
                    <div class="col-md-10">
                        <select readonly class="form-control" name="ruang" id="ruang">
                            <?php foreach ($form['ruang']['option'] as $item) { ?>
                                <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->ruangan; ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('ruang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['barang']['label'], 'barang', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="barang" id="barang">
                            <option value="">-- Pilih Barang --</option>
                            <?php foreach ($form['barang']['option'] as $item) { ?>
                                <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->nama_barang . ' ' . $item->tahun . ' | (' . $item->sisa . ') buah'; ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('barang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['uraian_maintenance']['label'], 'uraian_maintenance', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_textarea($form['uraian_maintenance']['input']) ?>
                        <?php echo form_error('uraian_maintenance', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['next_maintenance']['label'], 'next_maintenance', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['next_maintenance']['input']); ?>
                        <?php echo form_error('next_maintenance', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory/proses/maintenance'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>