<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
    <div class="col-md-12">
        <?php echo show_alert($this->session->flashdata()); ?>

        <div class="card">
            <?php echo form_open('', array('class' => 'form-horizontal')); ?>
            <div class="card-header">
                <i class="nav-icon icon-flag"></i> Proses Penempatan Inventaris
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <?php echo form_label($form['tanggal_penempatan']['label'], 'tanggal_penempatan', $form_label) ?>
                    <div class="col-md-10">
                        <?php echo form_input($form['tanggal_penempatan']['input']); ?>
                        <?php echo form_error('tanggal_penempatan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['penanggung_jawab']['label'], 'penanggung_jawab', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="penanggung_jawab" id="penanggung_jawab">
                            <option value="">-- Penanggung Jawab Pemindahan --</option>
                            <?php foreach ($form['penanggung_jawab']['option'] as $item) { ?>
                                <?php if ($form['penanggung_jawab']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->nip . ' | ' . $item->nama; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->nip . ' | ' . $item->nama; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('penanggung_jawab', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['ruang_asal']['label'], 'ruang_asal', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="ruang_asal" id="ruang_asal">
                            <option value="">-- Pilih Lokasi Barang --</option>
                            <?php foreach ($form['ruang_asal']['option'] as $item) { ?>
                                <?php if ($form['ruang_asal']['selected'] == $item->id) { ?>
                                    <option selected value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->ruangan; ?></option>
                                <?php } else {  ?>
                                    <option value="<?php echo $item->id; ?>"><?php echo $item->id . ' | ' . $item->ruangan; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <?php echo form_error('ruang_asal', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['barang']['label'], 'barang', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="barang" id="barang">
                            <option value="">-- Pilih Barang --</option>
                            <!-- Load dengan ajax-->
                        </select>
                        <?php echo form_error('barang', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['ruang_tujuan']['label'], 'ruang_tujuan', $form_label); ?>
                    <div class="col-md-10">
                        <select class="form-control app-select2" name="ruang_tujuan" id="ruang_tujuan">
                            <option value="">-- Pilih Ruang Tujuan --</option>
                            <!-- Load dengan ajax-->
                        </select>
                        <?php echo form_error('ruang_tujuan', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <?php echo form_label($form['jumlah']['label'], 'jumlah', $form_label) ?>
                    <div class="col-md-10">
                        <div id="lbl_jumlah" class="pl-2 text-info font-weight-bold small"></div>
                        <?php echo form_input($form['jumlah']['input']); ?>
                        <?php echo form_error('jumlah', '<label class="text-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="text-danger"><span class="font-weight-bold">Note</span> : (*) harus diisi</div>
            </div>

            <div class="card-footer">
                <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit')); ?>
                <?php echo anchor(base_url('inventory'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning')); ?>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>