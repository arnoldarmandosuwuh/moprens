<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Rekap Presensi Siswa
        <!-- <?php if($this->ion_auth->is_admin()) { ?>
          <div class="pull-right">
          <a href="<?php echo base_url('presensi_online/report/rekap/get_csv'); ?>" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Ekspor CSV</a>
          <a href="<?php echo base_url('presensi_online/report/rekap/read_csv'); ?>" class="btn btn-success btn-sm"><i class="fa fa-upload"></i> Impor CSV</a>
          <a href="<?php echo base_url('presensi_online/report/rekap/sinkron_presensi'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Impor Presensi</a>
        </div>
        <?php } ?> -->
      </div>
      <form class="form-horizontal" method="post" action="<?php echo base_url('presensi_online/report/rekap/'); ?>">

        <div class="row">

            <div class="card-body">
              <div class="col-md-12">
                
                <div class="form-group row">  
                  <label for="tanggal_awal" class="control-label col-md-2"> Tanggal Awal </label>
                  <div class="col-md-10">
                    <input class="form-control app-datepicker" id="tanggal_awal" name="tanggal_awal" placeholder="Masukkan Tanggal Awal">
                  </div>
                </div>

                <div class="form-group row">  
                  <label for="tanggal_akhir" class="control-label col-md-2"> Tanggal Akhir </label>
                  <div class="col-md-10">
                    <input class="form-control app-datepicker" id="tanggal_akhir" name="tanggal_akhir"  placeholder="Masukkan Tanggal Akhir">
                  </div>
                </div>
                <div class="form-group row">  
                  <label for="tanggal_akhir" class="control-label col-md-2"> Pencarian Lanjutan </label>
                </div>
                <div class="form-group row">  
                <select class="form-control app-select2 col-md-2" id="search" name="search">
                  <option value="">-- Pilih pencarian --</option>
                  <option value="terlambat">Terlambat lebih dari</option>
                  <option value="bolos">Tidak hadir lebih dari</option>
                  <option value="izin">Izin lebih dari</option>
                </select>
                  <div class="col-md-10">
                    <input class="form-control" id="key" name="key" type="number" placeholder="Jumlah">
                  </div>
                </div>

                <button class="btn btn-primary pull-right"  type="submit"><i class="fa fa-check"></i> Proses </button>

              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="row">
        <div class="card-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <p>REKAP PRESENSI KEHADIRAN MULAI TANGGAL <b><?php echo output_date($tanggal_awal->format('Y-m-d'))  ?></b> SAMPAI DENGAN TANGGAL <b><?php echo output_date($tanggal_akhir->format('Y-m-d')) ?></b>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="row">
          <div class="card-body">

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed table-datatable">
                  <thead>
                    <tr>
                        <th style="width: 10px;">No</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Hadir</th>
                        <th>Izin</th>
                        <th>Tidak Hadir</th>
                        <th>Terlambat</th>
                        <th>Pertemuan</th>
                        <th style="width: 10px;">Pilihan</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $no=1;
                        foreach($list_data as $nis => $presensi){ ?>
                            <tr>
                              <td><?php echo $no++; ?></td>
                              <td><pre><?php echo $nis ?></pre></td>
                              <td><?php echo $presensi['nama']; ?></td>
                              <td><?php echo $presensi['hadir']; ?></td>
                              <td><?php echo $presensi['izin']; ?></td>
                              <td><?php echo $presensi['bolos']; ?></td>
                              <td><?php echo $presensi['terlambat']; ?></td>
                              <td><?php echo $presensi['pertemuan']; ?></td>
                              <td>
                                <div class="dropdown dropleft">
                                  <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="icon-menu"></i>
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo base_url('presensi_online/report/presensi/presensi/'.$nis); ?>"><i class="fa fa-eye"></i> Lihat Jejak</a>
                                  </div>
                                </div>
                              </td>
                            </tr>
                      <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
