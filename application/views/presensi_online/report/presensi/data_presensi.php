<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Jejak Presensi Siswa
        <?php if($this->ion_auth->is_admin()) { ?>
        <div class="pull-right">
          <a href="<?php echo base_url('presensi_online/report/presensi/sinkron_presensi'); ?>" class="btn btn-success btn-sm"><i class="fa fa-refresh"></i> Update Data Presensi</a>
        </div>
        <?php } ?>
      </div>
      <?php if ($this->ion_auth->in_group('5')){
        $url = base_url('presensi_online/report/presensi/');
      } else {
        $url = base_url('presensi_online/report/presensi/presensi/'.$siswa['kode']);
      } ?>
      <form class="form-horizontal" method="post" action="<?php echo $url; ?>">

        <div class="row">

            <div class="card-body">
              <div class="col-md-12">
                
                <div class="form-group row">  
                  <label for="tanggal_awal" class="control-label col-md-2"> Tanggal Awal </label>
                  <div class="col-md-10">
                    <input class="form-control app-datepicker" id="tanggal_awal" name="tanggal_awal" placeholder="Masukkan Tanggal Awal">
                  </div>
                </div>

                <div class="form-group row">  
                  <label for="tanggal_akhir" class="control-label col-md-2"> Tanggal Akhir </label>
                  <div class="col-md-10">
                    <input class="form-control app-datepicker" id="tanggal_akhir" name="tanggal_akhir"  placeholder="Masukkan Tanggal Akhir">
                  </div>
                </div>

                <button class="btn btn-primary pull-right"  type="submit"><i class="fa fa-check"></i> Proses </button>

              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="row">
        <div class="card-body">
          <div class="col-md-12">
            <div class="row">
            <?php if($this->ion_auth->in_group('5')) { ?>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-3">
                    <p><b>NIS</b></p>
                  </div>
                  <div class="col-md-3">
                    <p><?php echo ' : '.$this->ion_auth->user()->row()->username; ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <p><b>Nama</b></p>
                  </div>
                  <div class="col-md-3">
                    <p><?php echo ' : '.$this->ion_auth->user()->row()->first_name; ?></p>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php if(!$this->ion_auth->in_group('5')) { ?>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-3">
                    <p><b>NIS</b></p>
                  </div>
                  <div class="col-md-3">
                    <p><?php echo ' : '.$siswa['kode'] ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <p><b>Nama</b></p>
                  </div>
                  <div class="col-md-3">
                    <p><?php echo ' : '.$siswa['nama'] ?></p>
                  </div>
                </div>
              </div>
              <?php } ?>
              <div class="col-md-6">
                <p>PRESENSI KEHADIRAN MULAI TANGGAL <b><?php echo output_date($tanggal_awal->format('Y-m-d'))  ?></b> SAMPAI DENGAN TANGGAL <b><?php echo output_date($tanggal_akhir->format('Y-m-d')) ?></b>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="row">
          <div class="card-body">

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed table-datatable">
                  <thead>
                    <tr>
                        <th style="width: 10px;">No</th>
                        <th>Hari</th>
                        <th>Tanggal</th>
                        <th>Jam Masuk</th>
                        <th>Jam Pulang</th>
                        <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $no=1; 
                        foreach($list_presensi as $key_presensi => $presensi){ ?>
                            <tr class="<?php echo $presensi['tr'] ?>">
                              <td><?php echo $no++; ?></td>
                              <td><pre><?php echo $presensi['hari']; ?></pre></td>
                              <td><?php echo output_date($presensi['tanggal']); ?></td>
                              <td><?php echo $presensi['checkin']; ?></td>
                              <td><?php echo $presensi['checkout']; ?></td>
                              <td><?php echo $presensi['ket']; ?></td>
                            </tr>
                      <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
