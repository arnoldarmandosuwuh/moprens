<div class="row">
    <div class="col-md-12">
        <div class="jumbotron bg-light">
            <div class="container">
                <h1 class="display-5 text-danger">Selamat Datang <?php echo $this->ion_auth->user()->row()->first_name; ?></h1>
                <?php if($this->ion_auth->in_group('5')){ ?>
                    <p class="lead text-danger">MoPrens - Monitoring Presensi Online</p>
                <?php } else {?>
                    <p class="lead text-danger">MoPrens - Monitoring Presensi Online - <?php echo  $this->ion_auth->get_users_groups()->row()->description ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php if(!$this->ion_auth->in_group('5')) { ?>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body">
                <div class="row justify-content-between">
                    <div class="col mx-auto">
                        <div class="text-value"><?php echo $siswa; ?></div>
                        <div>Siswa</div>
                    </div>
                    <div class="col-4">
                        <img src=<?php echo base_url('/assets/img/moprens/ab2.png') ?> height="50" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body">
                <div class="row justify-content-between">
                    <div class="col mx-auto">
                        <!-- <div class="text-value"><?php echo $list_presensi['bolos']; ?></div> -->
                        <div>Siswa Tidak Hadir</div>
                    </div>
                    <div class="col-4">
                        <img src=<?php echo base_url('/assets/img/moprens/ab1.png') ?> height="50" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body">
                <div class="row justify-content-between">
                    <div class="col mx-auto">
                        <!-- <div class="text-value"><?php echo $list_presensi['terlambat']; ?></div> -->
                        <div>Siswa Terlambat</div>
                    </div>
                    <div class="col-4">
                        <img src=<?php echo base_url('/assets/img/moprens/ab3.png') ?> height="50" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body">
                <div class="row justify-content-between">
                    <div class="col mx-auto">
                        <!-- <div class="text-value"><?php echo $list_presensi['izin']; ?></div> -->
                        <div>Siswa Izin</div>
                    </div>
                    <div class="col-4">
                        <img src=<?php echo base_url('/assets/img/moprens/ab3.png') ?> height="50" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else if($this->ion_auth->in_group('5')) {?>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body text-center">
                <h1><img src=<?php echo base_url('/assets/img/moprens/ab2.png') ?> height="50" /></h1>
                <h3>Total Pertemuan <?php echo $list_presensi['jmlpertemuan'] ?></h3>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body text-center">
                <h1><img src=<?php echo base_url('/assets/img/moprens/ab3.png') ?> height="50" /></h1>
                <h3>Total Terlambat <?php echo $list_presensi['jmlterlambat'] ?></h3>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body text-center">
                <h1><img src=<?php echo base_url('/assets/img/moprens/ab1.png') ?> height="50" /></h1>
                <h3>Total Izin <?php echo $list_presensi['jmlizin'] ?></h3>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-danger border-warning text-white">
            <div class="card-body text-center">
                <h1><img src=<?php echo base_url('/assets/img/moprens/ab1.png') ?> height="50" /></h1>
                <h3>Total Tidak Hadir <?php echo $list_presensi['jmlbolos'] ?></h3>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<style type="text/css">
</style>
