<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <form class="form-horizontal" method="post" action="<?php echo base_url('presensi_online/master/ambil/get_csv') ?>" enctype="multipart/form-data">
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Ekspor Presensi
      </div>
      <div class="card-body">
        
        <div class="form-group row">  
          <label for="tanggal_awal" class="control-label col-md-2"> Tanggal Awal </label>
          <div class="col-md-10">
            <input class="form-control app-datepicker" id="tanggal_awal" name="tanggal_awal" placeholder="Masukkan Tanggal Awal">
          </div>
        </div>

        <div class="form-group row">  
          <label for="tanggal_akhir" class="control-label col-md-2"> Tanggal Akhir </label>
          <div class="col-md-10">
            <input class="form-control app-datepicker" id="tanggal_akhir" name="tanggal_akhir"  placeholder="Masukkan Tanggal Akhir">
          </div>
        </div>

        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>        
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('presensi_online/dasboard'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

    </form>
    </div>
  </div>
</div>