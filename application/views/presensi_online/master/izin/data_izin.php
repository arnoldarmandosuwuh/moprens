<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Izin Siswa
        <?php if($this->ion_auth->is_admin()) { ?>
        <div class="pull-right">
          <a href="<?php echo base_url('presensi_online/master/izin/tambah'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <?php } ?>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed izin-datatable">
                <thead>
                  <tr>
                      <th style="width: 10px;">No</th>
                      <th>NIS</th>
                      <th>Nama</th>
                      <th>Tanggal Awal</th>
                      <th>Tanggal Akhir</th>
                      <th style="width: 10px;">Pilihan</th>
                  </tr>
                </thead>
                <tbody>
                    <!-- konten di-load secara ajax -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
