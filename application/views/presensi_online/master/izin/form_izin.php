<?php
$form_label = array('class' => 'control-label col-md-2');
?>
<div class="row">
  <div class="col-md-12">
    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <?php echo form_open('', array('class' => 'form-horizontal')); ?>
      <div class="card-header">
        <i class="nav-icon icon-flag"></i> Tambah Izin Siswa
      </div>
      <div class="card-body">
        <div class="form-group row">
        <?php echo form_label($form['nis']['label'],'nis', $form_label) ?>
          <div class="col-md-10">
            <?php  echo form_dropdown($form['nis']['name'],$form['nis']['option'],$form['nis']['selected'],$form['nis']['extra']); ?>
            <?php echo form_error('nis', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        
        <div class="form-group row">
        <?php echo form_label($form['tanggal_awal']['label'],'tanggal_awal', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tanggal_awal']['input']); ?>
            <?php echo form_error('tanggal_awal', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

      <div class="form-group row">
        <?php echo form_label($form['tanggal_akhir']['label'],'tanggal_akhir', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['tanggal_akhir']['input']); ?>
            <?php echo form_error('tanggal_akhir', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>

      <div class="form-group row">
        <?php echo form_label($form['keterangan']['label'],'keterangan', $form_label) ?>
          <div class="col-md-10">
            <?php echo form_input($form['keterangan']['input']); ?>
            <?php echo form_error('keterangan', '<label class="text-danger">', '</label>'); ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-10">
            <label class="text-danger">* : Wajib diisi.</label>
          </div>
        </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('presensi_online/master/izin'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
