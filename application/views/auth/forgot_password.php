<div class="row justify-content-center" style="margin-bottom: 20px;">
    <div class="col-md-8 text-center">
      <img src="<?php echo base_url('assets/img/brand/sistersmazaba.png'); ?>" style="width: 40%;"/>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        <?php if (!empty($message)) : ?>
          <div class="alert alert-danger mx-4">
            <?php echo $message; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php endif; ?>
        <div class="card mx-4">
            <?php echo form_open("auth/forgot_password");?>
            <div class="card-body p-4">
                <h2><?php echo lang('forgot_password_heading');?></h2>
                <p class="text-muted"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="icon-user"></i>
                        </span>
                    </div>
                    <?php echo form_input($identity);?>
                </div>
                <?php echo form_submit('submit', lang('forgot_password_submit_btn'), array('class' => 'btn btn-primary btn-block'));?>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
