<div class="row">
  <div class="col-md-12">
    <?php if (!empty($message_error)) : ?>
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?php echo $message_error; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>
    <div class="card">
      <?php echo form_open(uri_string(), array('class' => 'form-horizontal'));?>
      <div class="card-header">
        <i class="fa fa-user"></i> Ganti Password
      </div>
      <div class="card-body">
          <div class="form-group row">
            <label class="col-md-4 col-form-label"><?php echo lang('change_password_old_password_label', 'old_password');?></label>
            <div class="col-md-8">
              <?php echo form_input($old_password);?>
              <?php echo form_error('old', '<div class="invalid-feedback">', '</div>'); ?>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
            <div class="col-md-8">
              <?php echo form_input($new_password);?>
              <?php echo form_error('new', '<div class="invalid-feedback">', '</div>'); ?>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-4 col-form-label"><?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?></label>
            <div class="col-md-8">
              <?php echo form_input($new_password_confirm);?>
              <?php echo form_error('new_confirm', '<div class="invalid-feedback">', '</div>'); ?>
            </div>
          </div>
      </div>

      <div class="card-footer">
          <?php echo form_input($user_id);?>

          <?php echo form_button(array('content' => '<i class="fa fa-lock"></i> Ganti Password', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
