<div class="row">
  <div class="col-md-12">
    <div class="card">
      <?php echo form_open(uri_string(), array('class' => 'form-horizontal'));?>
      <div class="card-header">
        <i class="nav-icon icon-user"></i> Edit Pengguna
      </div>
      <div class="card-body">
              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_fname_label', 'first_name');?></label>
                <div class="col-md-4">
                  <?php echo form_input($first_name);?>
                  <?php echo form_error('first_name', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_lname_label', 'last_name');?></label>
                <div class="col-md-4">
                  <?php echo form_input($last_name);?>
                  <?php echo form_error('last_name', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_company_label', 'company');?></label>
                <div class="col-md-10">
                  <?php echo form_input($company);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_email_label', 'email');?> </label>
                <div class="col-md-10">
                  <?php echo form_input($email);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_phone_label', 'phone');?> </label>
                <div class="col-md-10">
                  <?php echo form_input($phone);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_password_label', 'password');?></label>
                <div class="col-md-10">
                  <?php echo form_input($password);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
                <div class="col-md-10">
                  <?php echo form_input($password_confirm);?>
                </div>
              </div>

              <?php if ($this->ion_auth->is_admin()): ?>
              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('edit_user_groups_heading');?></label>
                <div class="col-md-10">
                  <?php foreach ($groups as $group):?>
                      <div class="form-check form-check-inline mr-3">
                      <?php
                          $gID=$group['id'];
                          $checked = null;
                          $item = null;
                          foreach($currentGroups as $grp) {
                              if ($gID == $grp->id) {
                                  $checked= ' checked="checked"';
                              break;
                              }
                          }
                      ?>
                      <input type="checkbox" name="groups[]" id="<?php echo $group['id'];?>" class="form-check-input" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                      <label class="form-check-label" for="<?php echo $group['id'];?>"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                      </div>
                  <?php endforeach?>
                </div>
              </div>
              <?php endif ?>

              <?php echo form_hidden('id', $user->id);?>
              <?php echo form_hidden($csrf); ?>

      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('auth'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
