<div class="row">
  <div class="col-md-12">
    <div class="card">
      <?php echo form_open(uri_string(), array('class' => 'form-horizontal'));?>
      <div class="card-header">
        <i class="nav-icon icon-people"></i> Tambah Grup
      </div>
      <div class="card-body">
          <div class="form-group row">
            <label class="col-md-2 col-form-label"><?php echo lang('create_group_name_label', 'group_name');?></label>
            <div class="col-md-10">
              <?php echo form_input($group_name); ?>
              <?php echo form_error('group_name', '<div class="invalid-feedback">', '</div>'); ?>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-md-2 col-form-label"><?php echo lang('create_group_desc_label', 'description');?></label>
            <div class="col-md-10">
              <?php echo form_input($description);?>
              <?php echo form_error('description', '<div class="invalid-feedback">', '</div>'); ?>
            </div>
          </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('auth'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
