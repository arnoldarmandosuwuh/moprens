      <div class="row justify-content-center" style="margin-bottom: 20px;">
        <div class="col-md-8 text-center">
          <img src="<?php echo base_url('assets/img/brand/sistersmazaba.png'); ?>" style="width: 40%;"/>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-md-8">
          <?php if (!empty($message_error)) : ?>
            <div class="alert alert-danger">
              <?php echo $message_error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php endif; ?>

          <?php if (!empty($message_success)) : ?>
            <div class="alert alert-success">
              <?php echo $message_success; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php endif; ?>

          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <?php echo form_open('auth/login/'.$app, 'class="needs-validation"');?>
                <h1>Login</h1>
                <p class="text-muted">Masukkan username dan password Anda.</p>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <?php echo form_input($identity);?>
                  <?php echo form_error('identity', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <?php echo form_input($password);?>
                  <?php echo form_error('password', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <div class="row">
                  <div class="col-6">
                    <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary px-4"');?>
                  </div>
                  <div class="col-6 text-right">
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                    <?php echo lang('login_remember_label', 'remember');?>
                  </div>
                </div>
                <?php echo form_close();?>
              </div>
            </div>
            <div class="card text-white bg-primary py-5">
              <div class="card-body text-center">
                <div>
                  <h2>Lupa Password?</h2>
                  <p>Klik tombol berikut untuk me-reset password Anda, sistem kami akan mengirimkan tautan ke email Anda untuk me-reset password.</p>
                  <a class="btn btn-warning active mt-3" href="forgot_password">
                    Reset Password
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>