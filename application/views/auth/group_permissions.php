  <div class="row">
    <div class="col-md-12">
      
      <?php if (!empty($this->session->userdata('success'))) : ?>
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?php echo $this->session->userdata('success'); ?>
        </div>
        <?php endif; ?>

      <div class="card">
        <div class="card-header"><i class="fa fa-users"></i> Hak Akses Grup : <strong><?php echo $group->row()->name; ?></strong></div>
        
        
        <div class="card-body">
        <?php echo form_open(); ?>
        <?php foreach ($permissions as $key_permission => $permission) :?>
          
            <div class="card card-accent-primary">
              <div class="card-header">
                <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_permission); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_permission); ?>">
                  <i class="icon-grid"></i> <?php echo ucwords(str_replace("_", " ", $key_permission)); ?>
                </a>
                <div class="card-header-actions">
                  <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_permission); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_permission); ?>">
                    <i class="fa fa-chevron-down"></i>
                  </a>
                </div>
              </div>
              <div class="collapse" id="collapse_<?php echo str_replace(" ", "_", $key_permission); ?>">
                <div class="card-body">

              <?php foreach ($permission as $key_modul => $modul) :?>
                
                  <div class="card card-accent-warning">
                    <div class="card-header">
                      <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_modul); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                        <i class="icon-list"></i> <?php echo ucwords(str_replace("_", " ", $key_modul)); ?>
                      </a>
                      <div class="card-header-actions">
                        <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_modul); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                          <i class="fa fa-chevron-down"></i>
                        </a>
                      </div>
                    </div>
                    <div class="collapse" id="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                      <div class="card-body">
                      <?php foreach ($modul as $key_controller => $controller) :?>
                        <div class="card card-accent-success">
                          <div class="card-header">
                            <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>" aria-expanded="false" aria-controls="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                              <i class="icon-doc"></i> <?php echo ucwords((implode(" ", explode("_", $key_controller)))); ?>
                            </a>
                            <div class="card-header-actions">
                              <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>" aria-expanded="false" aria-controls="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                                <i class="fa fa-chevron-down"></i>
                              </a>
                            </div>
                          </div>
                          <div class="collapse" id="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="data-bidang">
                                  <thead>
                                    <tr>
                                      <th>Permission Name</th>
                                      <th>Permission Key</th>
                                      <th style="width: 10%;">Allow</th>
                                      <th style="width: 10%;">Deny</th>
                                      <th style="width: 10%;">Ignore</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($controller as $key_action => $v) : ?>
                                    <tr>
                                        <td><?php echo $v['name']; ?></td>
                                        <td><?php echo $v['key']; ?></td>
                                        <td>
                                            <div class="animated-radio-button">
                                                <label>
                                                    <?php echo form_radio("perm_{$v['id']}", '1', set_radio("perm_{$v['id']}", '1', ( array_key_exists($v['key'], $group_permissions) && $group_permissions[$v['key']]['value'] === TRUE ) ? TRUE : FALSE)); ?>
                                                    <span class="label-text">Allow</span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="animated-radio-button">
                                                <label>
                                                    <?php echo form_radio("perm_{$v['id']}", '0', set_radio("perm_{$v['id']}", '0', ( array_key_exists($v['key'], $group_permissions) && $group_permissions[$v['key']]['value'] != TRUE ) ? TRUE : FALSE)); ?>
                                                    <span class="label-text">Deny</span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="animated-radio-button">
                                                <label>
                                                    <?php echo form_radio("perm_{$v['id']}", 'X', set_radio("perm_{$v['id']}", 'X', ( ! array_key_exists($v['key'], $group_permissions) ) ? TRUE : FALSE)); ?>
                                                    <span class="label-text">Ignore</span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>

                        <?php endforeach; ?>
                        
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>

                </div>
              </div>
            </div>
          <?php endforeach; ?>
                <?php echo form_button(array('type'=>'submit'), '<i class="fa fa-save"></i> Simpan','class="btn btn-primary"'); ?>
                <a href="<?php echo base_url('auth') ?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i>Kembali</a>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>