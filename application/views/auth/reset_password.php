<div class="row justify-content-center" style="margin-bottom: 20px;">
    <div class="col-md-8 text-center">
      <img src="<?php echo base_url('assets/img/brand/sistersmazaba.png'); ?>" style="width: 40%;"/>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        <?php if (!empty($message_error)) : ?>
            <div class="alert alert-danger mx-4">
              <?php echo $message_error; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php endif; ?>
        <div class="card mx-4">
			<?php echo form_open('auth/reset_password/' . $code);?>
            <div class="card-body p-4">
				<h1><?php echo lang('reset_password_heading');?></h1>
                <label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
                
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="icon-lock"></i>
                        </span>
                    </div>
                    <?php echo form_input($new_password);?>
                    <?php echo form_error('new', '<div class="invalid-feedback">', '</div>');?>
                </div>

                <label for="new_password_confirm"><?php echo lang('reset_password_new_password_confirm_label');?></label>
                
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="icon-lock"></i>
                        </span>
                    </div>
                    <?php echo form_input($new_password_confirm); ?>
					<?php echo form_error('new_confirm', '<div class="invalid-feedback">', '</div>');?>
                </div>

				<?php echo form_input($user_id);?>
				<?php echo form_hidden($csrf); ?>
				<?php echo form_submit('submit', lang('reset_password_submit_btn'), array('class' => 'btn btn-primary btn-block'));?>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
