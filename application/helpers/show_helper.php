<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('show'))
{
    /**
     * fungsi echo untuk menampilkan string jika tidak kosong
     * untuk mencegah error "undefined variable"
     * @param  string $string string yang akan ditampilkan
     */
    function show($string)
    {
        if (isset($string)) {
            echo $string;
        }
    }
}

if ( ! function_exists('show_alert'))
{
    /**
     * fungsi echo untuk menampilkan string jika tidak kosong
     * untuk mencegah error "undefined variable"
     * @param  string $string string yang akan ditampilkan
     */
    function show_alert(array $message)
    {
        if (empty($message)) {
            return;
        }

        if (!empty($message['success'])) {
            $pesan        = $message['success'];
            $alert_class  = 'success';
        } elseif (!empty($message['error'])) {
            $pesan        = $message['error'];
            $alert_class  = 'danger';
        }

        if (empty($pesan)) {
            return;
        }

        echo '<div class="alert alert-'.$alert_class.' alert-dismissible fade show" role="alert">'.
                $pesan.
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
                  '<span aria-hidden="true">&times;</span>'.
                '</button>'.
              '</div>';
    }
}