<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Dashboard
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu (https://coreui.io/demo/#icons/simple-line-icons.html)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
| - permission : nama permission untuk hak akses menu
*/
$config['dashboard'] = array(
    'Beranda' => array(
        'icon' => 'nav-icon icon-home',
        'url' => base_url('dashboard'),
    ),

);


/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Frontend
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu menggunakan font awesome (https://fontawesome.com/v4.7.0/icons)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
*/
$config['frontend'] = array();
