<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permission extends App_Controller
{

    public function index()
    {
        $data['permissions'] = $this->ion_auth_acl->permissions('full');
        $data['title']       = 'Hak Akses';
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Hak Akses', 'Hak Akses');
        $this->layout->view('permission/permissions', $data);
    }

    public function add_permission()
    {
        $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
        $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

        $this->form_validation->set_message('required', 'Please enter a %s');

        if ($this->form_validation->run() === FALSE) {
            $data['perm_key'] = array(
                'name' => 'perm_key',
                'id' => 'perm_key',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_key'),
                'class' => 'form-control',
                'autofocus' => 'autofocus',
            );

            $data['perm_name'] = array(
                'name' => 'perm_name',
                'id' => 'perm_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_name'),
                'class' => 'form-control',
                'autofocus' => 'autofocus',
            );

            $data['title']   = 'Tambah Hak Akses';
            $data['message'] = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));

            $this->layout->view('permission/add_permission', $data);
        } else {
            $new_permission_id = $this->ion_auth_acl->create_permission($this->input->post('perm_key'), $this->input->post('perm_name'));
            if ($new_permission_id) {
                // check to see if we are creating the permission
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("/permission", 'refresh');
            }
        }
    }

    public function update_permission()
    {
        // if (!$this->ion_auth_acl->has_permission('pengaturan-hak_akses-update')) {
        //     $this->load->view('permission/denied', array());
        //     return;
        // }
        if ($this->input->post() && $this->input->post('cancel'))
            redirect(base_url("permission"), 'refresh');

        $permission_id  =   $this->uri->segment(3);

        if (!$permission_id) {
            $this->session->set_flashdata('message', "No permission ID passed");
            redirect(base_url("permission"), 'refresh');
        }

        $permission =   $this->ion_auth_acl->permission($permission_id);

        $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
        $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

        $this->form_validation->set_message('required', 'Please enter a %s');

        if ($this->form_validation->run() === FALSE) {

            $data['perm_key'] = array(
                'name' => 'perm_key',
                'id' => 'perm_key',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_key'),
                'class' => 'form-control',
                'value' => $permission->perm_key,
                'autofocus' => 'autofocus',
            );

            $data['perm_name'] = array(
                'name' => 'perm_name',
                'id' => 'perm_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('perm_name'),
                'class' => 'form-control',
                'value' => $permission->perm_name,
                'autofocus' => 'autofocus',
            );

            $data['title']   = 'Tambah Hak Akses';
            $data['message']    = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));
            $data['permission'] = $permission;

            $this->layout->view('permission/edit_permission', $data);
        } else {
            $additional_data    =   array(
                'perm_name' =>  $this->input->post('perm_name')
            );

            $update_permission = $this->ion_auth_acl->update_permission($permission_id, $this->input->post('perm_key'), $additional_data);
            if ($update_permission) {
                // check to see if we are creating the permission
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect(base_url("permission"), 'refresh');
            }
        }
    }

    public function delete_permission()
    {
        // if (!$this->ion_auth_acl->has_permission('pengaturan-hak_akses-delete')) {
        //     $this->load->view('permission/denied', array());
        //     return;
        // }
        $permission_id  =   $this->uri->segment(3);

        if (!$permission_id) {
            $this->session->set_flashdata('message', "No permission ID passed");
            redirect("permission/permissions", 'refresh');
        }

        if ($this->ion_auth_acl->remove_permission($permission_id)) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("permission", 'refresh');
        } else {
            echo $this->ion_auth_acl->messages();
        }
    }
}
