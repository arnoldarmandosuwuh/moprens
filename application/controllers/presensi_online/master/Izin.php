<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->layout->set_dashboard_menu('presensi_online');
        $this->load->model('presensi_online/master/izin_model');
    }

    public function index()
    {
        if(!$this->ion_auth_acl->has_permission('presensi_online>master>izin>read')){
            $this->load->view('permission/denied', array());
            return;
        }
        $data                = array('title' => 'Selamat Datang');
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Izin Siswa');

        $this->layout->add_js(base_url('assets/js/presensi_online/master/izin.js'));
        
        $this->layout->view('presensi_online/master/izin/data_izin', $data);
    }

    public function get_data_izin()
    {
        echo $this->izin_model->get_data_izin();
    }

    public function tambah()
    {
        if(!$this->ion_auth_acl->has_permission('presensi_online>master>izin>create')){
            $this->load->view('permission/denied', array());
            return;
        }
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Izin Siswa',
            'breadcrumbs' => $this->layout->get_breadcrumbs('Tambah Izin Siswa')
        );

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->izin_model->insert_izin($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('presensi_online/master/izin', 'refresh');
        } else {
            $this->layout->view('presensi_online/master/izin/form_izin', $data);
        }
    }

    public function edit($id)
    {
        if(!$this->ion_auth_acl->has_permission('presensi_online>master>izin>read')){
            $this->load->view('permission/denied', array());
            return;
        }
        if (!empty($this->input->post())) {
            $data = $this->input->post();
        } else {
            $data = $this->izin_model->get_izin_by_id($id);
        }

        $form = $this->_populate_form($data);
        
        $data  = array(
            'form'    => $form,
            'title' => 'Edit Izin Siswa',
            'url_form' => base_url('presensi_online/master/izin/edit/'.$id)
        );

        if ($this->form_validation->run()) {
            $data = $this->input->post();
            $this->izin_model->update_izin($id, $data);

            $this->session->set_flashdata('success', 'Data telah berhasil diedit.');
            redirect('presensi_online/master/izin', 'refresh');
        } else {
            $this->layout->view('presensi_online/master/izin/form_izin', $data);
        }
    }

    public function hapus($id)
    {
        if(!$this->ion_auth_acl->has_permission('presensi_online>master>izin>read')){
            $this->load->view('permission/denied', array());
            return;
        }
        $this->izin_model->hapus_izin($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('presensi_online/master/izin', 'refresh');
    }

    private function get_all_siswa(){
        $data = $this->izin_model->get_all_siswa()->result();
        
        foreach ($data as $key){
            $result[$key->kode] = $key->nama;
        }
        return $result;
    }

    public function _populate_form($data)
    {
        $form = array(
            'nis' => array(
                'label'  => 'Nama Siswa',
                'name' => 'nis',
                'option' => $this->get_all_siswa(),
                'selected' => isset($data['nis']) ? $data['nis']:'',
                'extra' => array(
                    'id'            => 'nis',
                    'class'         => 'form-control app-select2',
                )
            ),

            'keterangan' => array(
                'label'  => 'Keterangan',
                'input' => array(
                    'name'          => 'keterangan',
                    'id'            => 'keterangan',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Keterangan'
                )
            ),

            'tanggal_awal' => array(
                'label'  => 'Tanggal Awal',
                'input' => array(
                    'name'          => 'tanggal_awal',
                    'id'            => 'tanggal_awal',
                    'class'         => 'form-control app-datetimepicker',
                    'placeholder'   => 'Masukkan Tanggal Awal'
                )
            ),

            'tanggal_akhir' => array(
                'label'  => 'Tanggal Akhir',
                'input' => array(
                    'name'          => 'tanggal_akhir',
                    'id'            => 'tanggal_akhir',
                    'class'         => 'form-control app-datetimepicker',
                    'placeholder'   => 'Masukkan Tanggal Akhir'
                )
            ),
        );

        $this->form_validation->set_rules('nis', 'Nama Siswa', 'required');

        //jika form gagal disimpan, set nilai form dari data post
        foreach ($data as $key => $value) {

            $form[$key]['input']['value'] = $value;
        }

        return $form;
    }
}