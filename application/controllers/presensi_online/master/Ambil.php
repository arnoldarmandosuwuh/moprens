<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ambil extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if($this->ion_auth->in_group('5')){
            redirect(base_url('presensi_online/dashboard'));
        }
        $this->layout->set_dashboard_menu('presensi_online');
        $this->load->model('presensi_online/master/ambil_model');
    }

    public function index()
    {
        $data = array(
            'title' => 'Ekspor Presensi',
            'breadcrumbs' => $this->layout->get_breadcrumbs('Ekspor Presensi')
        );

        $this->layout->view('presensi_online/master/ambil/form_export', $data);
    }

    public function get_csv(){

        $tanggal_awal = new DateTime(date('Y-m-d 00:00:00'));

        if (!$this->input->post('tanggal_awal') == null){
            $tanggal_awal = new DateTime($this->input->post('tanggal_awal'));
        }

        $tanggal_akhir = new DateTime(date('Y-m-d 23:59:59'));

        if (!$this->input->post('tanggal_akhir') == null){
            $tanggal_akhir = new DateTime($this->input->post('tanggal_akhir'));
        }

        $myData = $this->ambil_model->get_presensi_access($tanggal_awal, $tanggal_akhir)->result();
 
        // file name
        $filename = 'presensi_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
 
        // file creation
        $file = fopen('php://output', 'w');
 
        $header = array("nis", "check_time", "check_type");
        // $header = array("nis","date","test");
        fputcsv($file, $header);
 
        foreach ($myData as $line){
            fputcsv($file,array($line->nis,$line->check_time,$line->check_type));
        }
 
        fclose($file);
        exit;
    }

}