<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->layout->set_dashboard_menu('presensi_online_siswa');
        $this->load->model('presensi_online/report/absensi_model');
    }

    public function index()
    {
        $data                = array('title' => 'Selamat Datang');
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Presensi Siswa');

        $tanggal_awal = new DateTime(date('Y-m-01 00:00:00'));

        if (!$this->input->post('tanggal_awal') == null){
            $tanggal_awal = new DateTime($this->input->post('tanggal_awal'));
        }
        $data['tanggal_awal'] = $tanggal_awal;

        $tanggal_akhir = new DateTime(date('Y-m-d 23:59:59'));
        if (!$this->input->post('tanggal_akhir') == null){
            $tanggal_akhir = new DateTime($this->input->post('tanggal_akhir'));
        }
        $data['tanggal_akhir'] = $tanggal_akhir;
        
        $list_libur = array();
        $libur = $this->absensi_model->get_hari_libur($tanggal_awal, $tanggal_akhir)->result_array();
        foreach($libur as $key){
            $list_libur[] = array(
                'libur_awal' => new DateTime($key['tanggal_awal']),
                'libur_akhir' => new DateTime($key['tanggal_akhir']),
            );
        }

        $nis = $this->ion_auth->user()->row()->username;

        $list_siswa = array();
        
        $siswa = $this->absensi_model->get_siswa($nis)->result_array();
        foreach ($siswa as $key => $value){
            $list_siswa[]=$value;
        }
        
        $list_data = array();

        foreach ($list_siswa as $key_siswa => $val_siswa){
            $jml_pertemuan = 0;
            $tanggal = clone $tanggal_awal;

            while ($tanggal->format('Ymd') <= $tanggal_akhir->format('Ymd')){
                if(!isset($list_data[$val_siswa['kode']]['nama'])){
                    $list_data[$val_siswa['kode']]['nama'] = $val_siswa['nama'];
                }
                if(!isset($list_data[$val_siswa['kode']]['hadir'])){
                    $list_data[$val_siswa['kode']]['hadir'] = 0;
                }
                if(!isset($list_data[$val_siswa['kode']]['izin'])){
                    $list_data[$val_siswa['kode']]['izin'] = 0;
                }
                if(!isset($list_data[$val_siswa['kode']]['bolos'])){
                    $list_data[$val_siswa['kode']]['bolos'] = 0;
                }
                if(!isset($list_data[$val_siswa['kode']]['pertemuan'])){
                    $list_data[$val_siswa['kode']]['pertemuan'] = 0;
                }
                if(!isset($list_data[$val_siswa['kode']]['terlambat'])){
                    $list_data[$val_siswa['kode']]['terlambat'] = 0;
                }

                $jam_awal = new DateTime($tanggal->format("Y-m-d 07:00:00"));
                $jam_akhir = new DateTime($tanggal->format("Y-m-d 15:00:00"));

                $jam_awal->sub(new DateInterval("PT90M"));
                $jam_akhir->add(new DateInterval("PT60M"));

                if ($tanggal->format("D") == 'Sat' || $tanggal->format("D") == 'Sun'){
                    $tanggal->add(new DateInterval("P1D"));
                    continue;
                }

                $is_libur = false;
                foreach($list_libur as $key_libur => $val_libur){
                    if($jam_awal->format("YmdHis") >= $val_libur['libur_awal']->format("YmdHis") && $jam_akhir->format("YmdHis") <= $val_libur['libur_akhir']->format("YmdHis")){
                        $is_libur = true;
                        break;
                    }
                }

                if($is_libur){
                    $tanggal->add(new DateInterval("P1D"));
                    continue;
                }

                $list_data[$val_siswa['kode']]['pertemuan']++;

                $presensi = $this->absensi_model->get_data_rekap($val_siswa['kode'], $jam_awal, $jam_akhir)->result_array();
                foreach ($presensi as $key_presensi){
                    $jm_presensi = $key_presensi['jumlah'];
                }
                
                if($jm_presensi <= 0){
                    $izin = $this->absensi_model->get_izin($val_siswa['kode'], $tanggal);
                    if ($izin->num_rows > 0){
                        $list_data[$val_siswa['kode']]['izin']++;
                    }
                    else {
                        $list_data[$val_siswa['kode']]['bolos']++;
                    }
                    $tanggal->add(new DateInterval("P1D"));
                    continue;
                }

                $list_absen = array();
                $presensi_terlambat = $this->absensi_model->get_data_presensi_id($val_siswa['kode'])->result_array();
                foreach($presensi_terlambat as $data_terlambat){
                    $index = date("Y-m-d", strtotime($data_terlambat['check_time']));
                    if(!empty($list_absen[$index][$data_terlambat['check_type']])){
                        continue;
                    }
                    $list_absen[$index][$data_terlambat['check_type']] = $data_terlambat['check_time'];
                }
                
                $checkin = !empty($list_absen[$tanggal->format("Y-m-d")]['I']) ? date("H:i:s", strtotime($list_absen[$tanggal->format("Y-m-d")]['I'])) : '-';
                
                if($checkin >= date('H:i:s', strtotime('07:00:00'))){
                    $list_data[$val_siswa['kode']]['terlambat']++;
                }

                $list_data[$val_siswa['kode']]['hadir']++;

                $tanggal->add(new DateInterval("P1D"));
            }
        }

        $data['list_data'] = $list_data;
        $this->layout->view('presensi_online/report/rekap/data_rekap_siswa', $data);
    }

}