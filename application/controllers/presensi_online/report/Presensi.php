<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->ion_auth->in_group('5')){
            $this->layout->set_dashboard_menu('presensi_online_siswa');
        }
        else {
            $this->layout->set_dashboard_menu('presensi_online');
        }
        $this->load->model('presensi_online/report/presensi_model');
    }

    public function index()
    {
        $data                = array('title' => 'Selamat Datang');
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Jejak Presensi Siswa');


        $list_hari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu',);
        
        $group = '5';
        $list_data = array();
        $nis = $this->ion_auth->user()->row()->username;
        
        $presensi = $this->presensi_model->get_data_presensi_id($nis)->result_array();

        foreach($presensi as $key){
            $index = date("Y-m-d", strtotime($key['check_time']));
            if(!empty($list_data[$index][$key['check_type']])){
                continue;
            }
            $list_data[$index][$key['check_type']] = $key['check_time'];
        }


        $tanggal_awal = new DateTime(date('Y-m-01 00:00:00'));

        if (!$this->input->post('tanggal_awal') == null){
            $tanggal_awal = new DateTime($this->input->post('tanggal_awal'));
        }
        $data['tanggal_awal'] = $tanggal_awal;

        $tanggal_akhir = new DateTime(date('Y-m-d 23:59:59'));
        if (!$this->input->post('tanggal_akhir') == null){
            $tanggal_akhir = new DateTime($this->input->post('tanggal_akhir'));
        }
        $data['tanggal_akhir'] = $tanggal_akhir;


        $data['list_presensi'] = array();
        
        $tanggal = clone $tanggal_awal;
        
        while ($tanggal_akhir->format("Ymd") >= $tanggal->format("Ymd")){
            if($tanggal->format("D") == 'Sun' || $tanggal->format("D") == 'Sat'){
                $tanggal->add(new DateInterval("P1D"));
                continue;
            }
            
            $ket = '';
            $izin = $this->presensi_model->get_izin($nis, $tanggal)->row_array();
    
            if(!empty($izin)){
                $ket = $izin['keterangan'];
            }

            $libur = $this->presensi_model->get_hari_libur($tanggal)->row_array();
            if(!empty($libur)){
                $ket = $libur['nama_hari_libur'];
            }

            if(empty($ket) && empty($list_data[$tanggal->format("Y-m-d")]['I']) && empty($list_data[$tanggal->format("Y-m-d")]['O'])){
                $ket = 'Tidak Hadir';               
            }

            $checkin = !empty($list_data[$tanggal->format("Y-m-d")]['I']) ? date("H:i:s", strtotime($list_data[$tanggal->format("Y-m-d")]['I'])) : '-';
            $checkout = !empty($list_data[$tanggal->format("Y-m-d")]['O']) ? date("H:i:s", strtotime($list_data[$tanggal->format("Y-m-d")]['O'])) : '-';

            $tr_class = '';

            if($checkin >= date('H:i:s', strtotime('07:00:00'))){
                $ket='Terlambat';
                $tr_class = 'table-info';
            }

            if (empty($list_data[$tanggal->format("Y-m-d")]['I']) && empty($list_data[$tanggal->format("Y-m-d")]['O'])){
                $tr_class = 'table-warning';
                if($ket == 'Tidak Hadir'){
                    $tr_class = 'table-danger';
                }
            }
        
            $data['list_presensi'][] = array (
                'hari' => $list_hari[$tanggal->format("w")],
                'tanggal' => $tanggal->format('Y-m-d'),
                'checkin' => $checkin,
                'checkout' => $checkout,
                'ket' => $ket,
                'tr' => $tr_class,
            );

            $tanggal->add(new DateInterval("P1D"));
        }
        
        $this->layout->view('presensi_online/report/presensi/data_presensi', $data);
    }

    function presensi($nis){
        $data                = array('title' => 'Selamat Datang');
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Jejak Presensi Siswa');

        $data['siswa'] = $this->presensi_model->get_siswa_by_id($nis);

        $list_hari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu',);
        
        $list_data = array();
        
        $presensi = $this->presensi_model->get_data_presensi_id($nis)->result_array();

        foreach($presensi as $key){
            $index = date("Y-m-d", strtotime($key['check_time']));
            if(!empty($list_data[$index][$key['check_type']])){
                continue;
            }
            $list_data[$index][$key['check_type']] = $key['check_time'];
        }

        $tanggal_awal = new DateTime(date('Y-m-01 00:00:00'));

        if (!$this->input->post('tanggal_awal') == null){
            $tanggal_awal = new DateTime($this->input->post('tanggal_awal'));
        }
        $data['tanggal_awal'] = $tanggal_awal;

        $tanggal_akhir = new DateTime(date('Y-m-d 23:59:59'));
        if (!$this->input->post('tanggal_akhir') == null){
            $tanggal_akhir = new DateTime($this->input->post('tanggal_akhir'));
        }
        $data['tanggal_akhir'] = $tanggal_akhir;


        $data['list_presensi'] = array();
        
        $tanggal = clone $tanggal_awal;
        
        while ($tanggal_akhir->format("Ymd") >= $tanggal->format("Ymd")){
            if($tanggal->format("D") == 'Sun' || $tanggal->format("D") == 'Sat'){
                $tanggal->add(new DateInterval("P1D"));
                continue;
            }
            
            $ket = '';
            $izin = $this->presensi_model->get_izin($nis, $tanggal)->row_array();
    
            if(!empty($izin)){
                $ket = $izin['keterangan'];
            }

            $libur = $this->presensi_model->get_hari_libur($tanggal)->row_array();
            if(!empty($libur)){
                $ket = $libur['nama_hari_libur'];
            }

            if(empty($ket) && empty($list_data[$tanggal->format("Y-m-d")]['I']) && empty($list_data[$tanggal->format("Y-m-d")]['O'])){
                $ket = 'Tidak Hadir';               
            }

            $checkin = !empty($list_data[$tanggal->format("Y-m-d")]['I']) ? date("H:i:s", strtotime($list_data[$tanggal->format("Y-m-d")]['I'])) : '-';
            $checkout = !empty($list_data[$tanggal->format("Y-m-d")]['O']) ? date("H:i:s", strtotime($list_data[$tanggal->format("Y-m-d")]['O'])) : '-';

            $tr_class = '';

            if($checkin >= date('H:i:s', strtotime('07:00:00'))){
                $ket='Terlambat';
                $tr_class = 'table-info';
            }

            if (empty($list_data[$tanggal->format("Y-m-d")]['I']) && empty($list_data[$tanggal->format("Y-m-d")]['O'])){
                $tr_class = 'table-warning';
                if($ket == 'Tidak Hadir'){
                    $tr_class = 'table-danger';
                }
            }
        
            $data['list_presensi'][] = array (
                'hari' => $list_hari[$tanggal->format("w")],
                'tanggal' => $tanggal->format('Y-m-d'),
                'checkin' => $checkin,
                'checkout' => $checkout,
                'ket' => $ket,
                'tr' => $tr_class,
            );

            $tanggal->add(new DateInterval("P1D"));
        }
        
        $this->layout->view('presensi_online/report/presensi/data_presensi', $data);
    }

    public function get_data_presensi()
    {
        echo $this->presensi_model->get_data_presensi();
    }

    public function sinkron_presensi(){
        $presensi = array();

        $presensi_sql = $this->presensi_model->get_presensi_access();

        $presensi = $presensi_sql->result_array();

        if($presensi_sql->num_rows() > 0){
            $this->presensi_model->hapus_presensi();
            //$this->presensi_model->hapus_presensi_host();
            foreach ($presensi as $sql => $value){
                if ($this->presensi_model->insert_presensi($value)){
                   // $this->presensi_model->insert_presensi_host($value);
                    $this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
                }
            }
            redirect('presensi_online/report/presensi', 'refresh');
        }
    }


}