<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('siswa_model');
    }

    public function index()
    {
        $data                = array('title' => 'Selamat Datang');
        $data['breadcrumbs'] = $this->layout->get_breadcrumbs('Data Siswa');

        $this->layout->add_js(base_url('assets/js/siswa.js'));
        
        $this->layout->view('siswa/data_siswa', $data);
    }

    public function get_data_siswa()
    {
        echo $this->siswa_model->get_data_siswa();
    }

    public function get_siswa(){
        $siswa = $this->siswa_model->get_all_data_siswa();
        echo json_encode($siswa->result());
    }

    public function tambah()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Siswa',
            'breadcrumbs' => $this->layout->get_breadcrumbs('Tambah Siswa')
        );

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->siswa_model->insert_siswa($post);

            $email = $this->input->post('kode').'@example.com';
            $username = $this->input->post('kode');
            $password = $this->input->post('kode');
            $additional_data = [
				'first_name' => $this->input->post('nama'),
				'company' => 'SISWA',
            ];
            $group = array('5');
            $this->ion_auth->register($username, $password, $email, $additional_data, $group);
            $tanggal = new DateTime();
            $tanggal->add(new DateInterval("P3Y"));
            $list_siswa = array(
                'nomor_identitas' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'alamat' => $this->input->post('alamat'),
                'foto' => $this->input->post('foto'),
                'kategori_anggota' => 'siswa',
                'status' => 'aktif',
                'tanggal_expired' => $tanggal->format("Y-m-d"),
                'deleted' => 0,
            );
            $this->siswa_model->insert_anggota($list_siswa);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('siswa', 'refresh');
        } else {
            $this->layout->view('siswa/form_siswa', $data);
        }
    }

    public function sinkronisasi (){
        //Declare array
        $siswa = array();

        //Data
        $siswa_sql = $this->siswa_model->get_all_siswa();

        //Array
        $siswa = $siswa_sql->result_array();

        if ($siswa_sql->num_rows() > 0){
            $this->siswa_model->delete_siswa_access();  

            foreach ($siswa as $sql) {
                $this->siswa_model->add_siswa_access($sql['kode'], $sql['kode'], $sql['nama']);
            }
            $this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
            redirect('siswa', 'refresh');
        }
    }

    public function get_csv (){

        $myData = $this->siswa_model->get_all_siswa()->result();
 
        // file name
        $filename = 'siswa_'.date('Ymd').'.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
 
        // file creation
        $file = fopen('php://output', 'w');
 
        $header = array("id","kode", "nama", "is_deleted");
        fputcsv($file, $header);
 
        foreach ($myData as $line){
            fputcsv($file,array($line->id,$line->kode,$line->nama,$line->is_deleted));
        }
 
        fclose($file);
        exit;
    }

    function read_csv()
    {
        $data = array(
            'title' => 'Import Siswa',
            'breadcrumbs' => $this->layout->get_breadcrumbs('Import Siswa')
        );
        $this->load->library('csvreader');
        if(!empty($_FILES['csv']['name'])){
            $filename = $_FILES['csv']['tmp_name'];
            $result =   $this->csvreader->parse_file($filename);//path to csv file
            
            //$this->siswa_model->delete_siswa_access();
            foreach($result as $key){
                $siswa = $this->siswa_model->check_siswa_by_id($key['kode']);
                
                if($siswa->num_rows() > 0){
                    continue;
                } else {
                    if ($this->siswa_model->add_siswa_access($key['id'], $key['kode'], $key['nama'])){
                        $this->session->set_flashdata('success', 'Import data telah berhasil.');
                    }
                }
                
            }
        redirect('dashboard', 'refresh'); 
        }
        else{
            $this->layout->view('siswa/form_import', $data);
        }
      
    }


    public function sinkron_anggota(){
        $siswa = array();
		$anggota = array();
		
        $siswa_sql = $this->siswa_model->get_all_siswa();
		$anggota_sql = $this->siswa_model->get_all_data_anggota();
		
        $siswa = $siswa_sql->result_array();
		$anggota = $anggota_sql->result_array();

        if($siswa_sql->num_rows() > 0){
            //$this->siswa_model->hapus_anggota();
            $tanggal = new DateTime();
            $tanggal->add(new DateInterval("P3Y"));
            foreach ($siswa as $sql){
				if($anggota_sql->num_rows()==0){
					$list_siswa = array(
						'nomor_identitas' => $sql['kode'],
						'nama' => $sql['nama'],
						'tempat_lahir' => $sql['tempat_lahir'],
						'tanggal_lahir' => $sql['tanggal_lahir'],
						'alamat' => $sql['alamat'],
						'foto' => $sql['foto'],
						'kategori_anggota' => 'siswa',
						'status' => 'aktif',
						'tanggal_expired' => $tanggal->format("Y-m-d"),
						'deleted' => 0,
					);
					if ($this->siswa_model->insert_anggota($list_siswa)){
						$this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
					}
				}
				else{
					$list_siswa = array(
						'nomor_identitas' => $sql['kode'],
						'nama' => $sql['nama'],
						'tempat_lahir' => $sql['tempat_lahir'],
						'tanggal_lahir' => $sql['tanggal_lahir'],
						'alamat' => $sql['alamat'],
						'foto' => $sql['foto'],
						'kategori_anggota' => 'siswa',
						'status' => 'aktif',
						'tanggal_expired' => $tanggal->format("Y-m-d"),
						'deleted' => 0,
					);
					$cek_nomor_identitas = 0;
					foreach($anggota as $key => $item){
						if($list_siswa['nomor_identitas'] == $item['nomor_identitas']){
							$cek_nomor_identitas++;
						}
					}
					if($cek_nomor_identitas == 0){
						if ($this->siswa_model->insert_anggota($list_siswa)){
							$this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
						}
					}
				}
            }
            redirect('siswa', 'refresh');
        }
    }

    public function sinkron_user(){
        $siswa = array();

        $siswa_sql = $this->siswa_model->get_all_siswa();

        $siswa = $siswa_sql->result_array();

        if($siswa_sql->num_rows() > 0){
            foreach ($siswa as $key){
                $email = $key['kode'].'@example.com';
                $username = $key['kode'];
                $password = $key['kode'];
                $additional_data = [
                    'first_name' => $key['nama'],
                    'company' => 'SISWA',
                ];
                $group = array('5');
                $this->ion_auth->register($username, $password, $email, $additional_data, $group);
    
            }
            $this->session->set_flashdata('success', 'Sinkronisasi data telah berhasil.');
        }
        redirect('siswa', 'refresh');
    }

    public function edit($id)
    {
        if (!empty($this->input->post())) {
            $data = $this->input->post();
        } else {
            $data = $this->siswa_model->get_siswa_by_id($id);
        }

        $form = $this->_populate_form($data);
        
        $data  = array(
            'form'    => $form,
            'title' => 'Edit Siswa',
            'url_form' => base_url('siswa/edit/'.$id)
        );

        if ($this->form_validation->run()) {
            $data = $this->input->post();
            $this->siswa_model->update_siswa($id, $data);

            $this->session->set_flashdata('success', 'Data telah berhasil diedit.');
            redirect('siswa', 'refresh');
        } else {
            $this->layout->view('siswa/form_siswa', $data);
        }
    }

    public function hapus($id)
    {
        $this->siswa_model->hapus_siswa($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('siswa', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'kode' => array(
                'label'  => 'NIS'.$req_sign,
                'input' => array(
                    'name'          => 'kode',
                    'id'            => 'kode',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan NIS'
                )
            ),

            'nama' => array(
                'label'  => 'Nama Siswa'.$req_sign,
                'input' => array(
                    'name'          => 'nama',
                    'id'            => 'nama',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Nama Siswa'
                )
            ),

            'jenis_kelamin' => array(
                'label'  => 'Jenis Kelamin',
                'input' => array(
                    'name'          => 'jenis_kelamin',
                    'id'            => 'jenis_kelamin',
                    'class'         => 'form-control app-select2',
                    'options'        => array(
                        'Laki-laki' => 'Laki-laki',
                        'Perempuan' => 'Perempuan',
                    )
                )
            ),

            'tempat_lahir' => array(
                'label'  => 'Tempat Lahir',
                'input' => array(
                    'name'          => 'tempat_lahir',
                    'id'            => 'tempat_lahir',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tempat Lahir'
                )
            ),

            'tanggal_lahir' => array(
                'label'  => 'Tanggal Lahir',
                'input' => array(
                    'name'          => 'tanggal_lahir',
                    'id'            => 'tanggal_lahir',
                    'class'         => 'form-control app-datepicker',
                    'type'          => 'date',
                    'placeholder'   => 'Masukkan Tanggal Lahir'
                )
            ),
            
            'alamat' => array(
                'label'  => 'Alamat',
                'input' => array(
                    'name'          => 'alamat',
                    'id'            => 'alamat',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Alamat'
                )
            ),

            'foto' => array(
                'label'  => 'Foto',
                'input' => array(
                    'name'          => 'foto',
                    'id'            => 'foto',
                    'class'         => 'form-control-file',
                    'type'          => 'file',
                    'accept'        => 'image/*',
                    'value'         => isset($data['foto']) ? base_url($data['foto']) : '/assets/img/avatars/user_default.png'
                )
            ),

            'keterangan' => array(
                'label'  => 'Keterangan',
                'input' => array(
                    'name'          => 'keterangan',
                    'id'            => 'keterangan',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Keterangan Siswa'
                )
            ),
            
            'is_deleted' => array(
                'input'  => array(
                    'value' => 0,
                ),
            ),
        );

        $this->form_validation->set_rules('kode', 'NIS', 'required');
        $this->form_validation->set_rules('nama', 'Nama Siswa', 'required');

        //jika form gagal disimpan, set nilai form dari data post
        foreach ($data as $key => $value) {
            $form_select = array ('jenis_kelamin');
            if (in_array($key, $form_select)){
                $form[$key]['input']['selected'] = $value;
            }
            else {
               $form[$key]['input']['value'] = $value;
            }
        }

        return $form;
    }
}