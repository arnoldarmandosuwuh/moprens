<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_model extends CI_Model {

    private $table = 'siswa';

    public function __construct()
    {
        $this->load->library('datatables');
        $this->datatables->set_database('default');
    }

    public function get_data_siswa()
    {
        $buttons = anchor(
                        base_url("master/anggota/edit/$1"),
                        '<i class="fa fa-edit"></i>',
                        array('class'=>'label label-warning')
                   ).' '.
                   anchor(
                        base_url('master/anggota/hapus/$1'),
                        '<i class="fa fa-trash"></i>',
                        array('class'=>'label label-danger btn-delete', 'onclick' => "return confirm('Apakah Anda yakin akan menghapus data?')")
                   );
        $this->datatables->add_column('pilihan', $buttons, 'id, foto');
        $this->datatables->add_column('nomor', '');

        $this->datatables->select('id, kode, nama, tempat_lahir, tanggal_lahir');
        $this->datatables->where('is_deleted = 0');
        $this->datatables->from($this->table);

        $data = json_decode($this->datatables->generate(), TRUE);
        
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['tanggal_lahir'] = output_date($value['tanggal_lahir'], TRUE);
        }

        return json_encode($data);
    }

    public function get_all_siswa(){
        return $this->db->get($this->table);
    }

	public function get_all_data_anggota(){
        $db2 = $this->load->database('simpus', TRUE);
        return $db2->get('anggota');
    }
	

    public function add_siswa_access($id, $nis, $name)
    {
        $db2 = $this->load->database('access', TRUE);
        $replacements= array(
            "'" => "''",
        );
        $nama = strtr($name, $replacements);
        $sql = $db2->query("INSERT INTO `USERINFO` (`USERID`, `Badgenumber`, `SSN`, `Name`) VALUES ('".$id."', '".$nis."', '".$nis."', '".$nama."') ");
        return $sql;
    }

    public function check_siswa_by_id($id){
        $db2 = $this->load->database('access', TRUE);

        $sql = $db2->query("SELECT `USERID`, `SSN`, `Name` ".
        "FROM `USERINFO` WHERE `SSN` = '".$id."'");
        return $sql;
    }

    public function get_siswa_access(){
        $db2 = $this->load->database('access', TRUE);

        $sql = $db2->query("SELECT `USERID`, `SSN`, `Name` ".
        "FROM `USERINFO` WHERE not isNull(`SSN`) ORDER BY `SSN` asc");
        return $sql;
    }

    public function delete_siswa_access()
    {
        $db2 = $this->load->database('access', TRUE);
    
        $sql = $db2->query("DELETE FROM `USERINFO`");
        return $sql;
    }

    public function add_siswa_lokal($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function insert_siswa($data)
    {
        if (!empty($_FILES['foto']['name']))
        {
            $tgl = date("Y_m_d");
            $dir = '/assets/upload/foto/siswa/';
            $config['upload_path'] = './assets/upload/foto/siswa/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';    
            $config['overwrite'] = true;
            $config['file_name'] = $tgl.'_'.$this->input->post('kode'); 
            $this->load->library('upload', $config);

            $this->upload->initialize($config);
            if ($this->upload->do_upload('foto'))
            {
                $img = $this->upload->data();
                $data['foto'] = $dir.$img['file_name'];

         
                return $this->db->insert($this->table, $data);
            }
            else
            {
                return false;

            }
        }
        else {
            return $this->db->insert($this->table, $data);
        }
    }

    public function insert_anggota($data)
    {
        $db2 = $this->load->database('simpus', TRUE);
    
        return $db2->insert('anggota', $data);
    }

    public function hapus_anggota()
    {
        $db2 = $this->load->database('simpus', TRUE);
    
        $db2->query('DELETE FROM `anggota`');
    }

    public function hapus_siswa($id)
    {
        $this->db->set('is_deleted', 1);
        $this->db->where('id', $id);
        $this->db->update($this->table);
    }

    public function get_csv(){
        $tanggal = date("Y-m-d H-i-s");
        $filename = 'D:/'.$tanggal.' - siswa.csv';
        $query = "SELECT * INTO OUTFILE '$filename' FIELDS TERMINATED BY ',' ".
        "OPTIONALLY ENCLOSED BY '' LINES TERMINATED BY '\r\n' FROM ".
        "( SELECT 'id' as `id`, 'kode' AS `kode`, 'nama' AS `nama`, 'is_deleted' AS `is_deleted` ".
        "UNION ALL ".
        "SELECT `id`, `kode`, `nama`, `is_deleted` FROM `siswa` ".
        ") `sub_query`";
        return $this->db->query($query);
    }

    public function hapus_siswa_lokal()
    {
        $this->db->query("DELETE FROM `siswa`");
    }

    public function get_siswa_by_id($id)
    {
        return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function update_siswa($id, $data)
    {
        if (!empty($_FILES['foto']['name']))
        {
            $tgl = date("Y_m_d");
            $dir = '/assets/upload/foto/siswa/';
            $config['upload_path'] = './assets/upload/foto/siswa/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';    
            $config['overwrite'] = true;
            $config['file_name'] = $tgl.'_'.$this->input->post('kode'); 
            $this->load->library('upload', $config);

            $this->upload->initialize($config);
            if ($this->upload->do_upload('foto'))
            {
                $img = $this->upload->data();
                $data['foto'] = $dir.$img['file_name'];

         
                $this->db->where('id', $id);
                $this->db->update($this->table, $data);
        
                return $this->db->affected_rows();
            }
            else
            {
                return false;

            }
        }
        else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
    
            return $this->db->affected_rows();
        }

    }
}