<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin_model extends CI_Model {

    private $table = 'izin_siswa';
    private $table2 = 'siswa';

    public function __construct()
    {
        $this->load->library('datatables');
        $this->datatables->set_database('presensi_online');
    }

    public function get_data_izin()
    {
        $db2 = $this->load->database('default', TRUE);
     
        $buttons = anchor(
                        base_url("master/anggota/edit/$1"),
                        '<i class="fa fa-edit"></i>',
                        array('class'=>'label label-warning')
                   ).' '.
                   anchor(
                        base_url('master/anggota/hapus/$1'),
                        '<i class="fa fa-trash"></i>',
                        array('class'=>'label label-danger btn-delete', 'onclick' => "return confirm('Apakah Anda yakin akan menghapus data?')")
                   );
        $this->datatables->add_column('pilihan', $buttons, 'id, foto');
        $this->datatables->add_column('nomor', '');

        $this->datatables->select('izin.id, izin.nis, siswa.nama as nama, izin.tanggal_awal, izin.tanggal_akhir');
        $this->datatables->from($this->table.' as izin');
        $this->datatables->join($db2->database.'.siswa', 'siswa.kode=izin.nis');

        $data = json_decode($this->datatables->generate(), TRUE);
        
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['tanggal_awal'] = output_date($value['tanggal_awal'], TRUE);
            $data['data'][$key]['tanggal_akhir'] = output_date($value['tanggal_akhir'], TRUE);
        }

        return json_encode($data);
    }

    public function insert_izin($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function hapus_izin($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get_izin_by_id($id)
    {
        return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function get_all_siswa()
    {
        $db2 = $this->load->database('default', TRUE);
        return $db2->get($this->table2);
    }

    public function update_izin($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);

        return $this->db->affected_rows();
    }
    
}