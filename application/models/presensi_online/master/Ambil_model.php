<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ambil_model extends CI_Model {

    private $table = 'presensi';

    public function insert_presensi($data)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        return $db2->insert($this->table, $data);
    }

    public function hapus_presensi()
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->query("DELETE FROM `presensi`");
    }

    public function get_presensi_access($tanggal_awal, $tanggal_akhir){
        $db2 = $this->load->database('access', TRUE);

        $sql = $db2->query("SELECT u.SSN as nis, c.CHECKTYPE as check_type, max(c.CHECKTIME) as check_time ".
        "FROM CHECKINOUT c, USERINFO u WHERE u.USERID=c.USERID AND ".
        "FORMAT(c.CHECKTIME,'yyyy-mm-dd') >= '".$tanggal_awal->format("Y-m-d")."' AND ".
        "FORMAT(c.CHECKTIME,'yyyy-mm-dd') <= '".$tanggal_akhir->format("Y-m-d")."' ".
        "GROUP BY FORMAT(c.CHECKTIME,'yyyy-mm-dd'), u.SSN,  c.CHECKTYPE ".
        "ORDER BY u.SSN ASC");
        return $sql;
    }

}