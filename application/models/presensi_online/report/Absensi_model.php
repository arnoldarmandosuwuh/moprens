<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi_model extends CI_Model {

    private $table = 'presensi';

    public function get_data_presensi()
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        return $db2->get($this->table);
    }

    public function get_data_presensi_id($nis)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->where('nis', $nis);
        $db2->where('check_type', 'I');
        return $db2->get($this->table);
    }

    public function get_data_rekap($nis, $jam_awal, $jam_akhir)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $query = $db2->query("SELECT COUNT(id) AS jumlah FROM `presensi` WHERE ".
        "`check_time` >= '".$jam_awal->format("Y-m-d H:i:s")."' AND ".
        "`check_time` <= '".$jam_akhir->format("Y-m-d H:i:s")."' AND ".
        "`nis` = '".$nis."'");
        return $query;
    }
    public function get_presensi_access(){
        $db2 = $this->load->database('access', TRUE);

        $sql = $db2->query("SELECT USERID as nis, CHECKTIME as check_time, CHECKTYPE as check_type ".
        "FROM CHECKINOUT ORDER BY USERID ASC, CHECKTIME DESC");
        return $sql;
    }
    
    public function insert_presensi($data)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        return $db2->insert($this->table, $data);
    }

    public function insert_presensi_host($data)
    {
        $db2 = $this->load->database('db_online2', TRUE);
        return $db2->insert($this->table, $data);
    }

    public function hapus_presensi()
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->query("DELETE FROM `presensi`");
    }

    public function hapus_presensi_host()
    {
        $db2 = $this->load->database('db_online2', TRUE);
        $db2->query("DELETE FROM `presensi`");
    }

    public function get_siswa($nis)
    {
        $this->db->where('kode',$nis);
        return $this->db->get('siswa');
    }

    public function update_siswa($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);

        return $this->db->affected_rows();
    }
    
    public function get_hari_libur($tanggal_awal, $tanggal_akhir){
        $query = $this->db->query("SELECT * FROM `hari_libur` WHERE ".
        "(tanggal_awal >= '".$tanggal_awal->format("Y-m-d H:i:s")."' AND tanggal_awal <= '".$tanggal_akhir->format("Y-m-d H:i:s")."') OR ".
        "(tanggal_akhir >= '".$tanggal_awal->format("Y-m-d H:i:s")."' AND tanggal_akhir <= '".$tanggal_akhir->format("Y-m-d H:i:s")."') OR ".
        "(tanggal_awal <= '".$tanggal_awal->format("Y-m-d H:i:s")."' AND tanggal_akhir >= '".$tanggal_awal->format("Y-m-d H:i:s")."')");

        return $query;
    }

    public function get_izin($nis, $tanggal){
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->where('nis = ', $nis);
        $db2->where('tanggal_awal <= ', $tanggal->format("Y-m-d"));
        $db2->where('tanggal_akhir >= ', $tanggal->format("Y-m-d"));
        $db2->order_by('id', 'asc');
       
        return $db2->get('izin_siswa');
    }
}