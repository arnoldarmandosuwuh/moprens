<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_model extends CI_Model {

    private $table = 'presensi';

    public function get_data_presensi()
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        return $db2->get($this->table);
    }

    public function get_data_presensi_id($nis)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->where('nis', $nis);
        return $db2->get($this->table);
    }
    public function get_presensi_access(){
        $db2 = $this->load->database('access', TRUE);

        $sql = $db2->query("SELECT USERID as nis, CHECKTIME as check_time, CHECKTYPE as check_type ".
        "FROM CHECKINOUT ORDER BY USERID ASC, CHECKTIME DESC");
        return $sql;
    }
    
    public function insert_presensi($data)
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        return $db2->insert($this->table, $data);
    }

    public function insert_presensi_host($data)
    {
        $db2 = $this->load->database('db_online2', TRUE);
        return $db2->insert($this->table, $data);
    }

    public function hapus_presensi()
    {
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->query("DELETE FROM `presensi`");
    }

    public function hapus_presensi_host()
    {
        $db2 = $this->load->database('db_online2', TRUE);
        $db2->query("DELETE FROM `presensi`");
    }

    public function hapus_siswa_lokal()
    {
        $this->db->query("DELETE FROM `siswa`");
    }

    public function get_siswa_by_id($id)
    {
        return $this->db->get_where('siswa', array('kode' => $id))->row_array();
    }

    public function update_siswa($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);

        return $this->db->affected_rows();
    }
    
    public function get_hari_libur($tanggal){
        $this->db->where('tanggal_awal <= ',$tanggal->format("Y-m-d"));
        $this->db->where('tanggal_akhir >= ',$tanggal->format("Y-m-d"));
        $this->db->order_by('id', 'asc');

        return $this->db->get('hari_libur');
    }

    public function get_izin($nis, $tanggal){
        $db2 = $this->load->database('presensi_online', TRUE);
        $db2->where('nis = ', $nis);
        $db2->where('tanggal_awal <= ', $tanggal->format("Y-m-d"));
        $db2->where('tanggal_akhir >= ', $tanggal->format("Y-m-d"));
        $db2->order_by('id', 'asc');
       
        return $db2->get('izin_siswa');
    }
}