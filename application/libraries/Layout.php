<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Template : untuk meng-handle layout aplikasi
 */
class Layout {

    /**
     * instance dari CodeIgniter
     * @var object
     */
    private $CI;

    /**
     * template data untuk memuat data yang akan ditampilkan di layout
     * @var array
     */
    private $template_data = array();

    /**
     * list file js yang akan di-load ke layout
     * @var array
     */
    protected $list_js = array();

    /**
     * list file css yang akan di-load ke layout
     * @var array
     */
    protected $list_css = array();

    /**
     * nama menu modul yang akan di muat ke template
     * default-nya menu dashboard
     * @var string
     */
    protected $dashboard_menu = 'dashboard';

    /**
     * konstruktor class Template
     */
    public function __construct()
    {
        $this->CI = get_instance();
        $this->CI->load->helper('active_menu');

        $this->init_list_css();
        $this->init_list_js();
    }

    /**
     * fungsi untuk memasukkan value ke dalam suatu bagian layout
     * @param string $content_area  nama content area
     * @param mixed  $value         value yang akan ditampilkan
     */
    public function set($content_area, $value)
    {
        $this->template_data[$content_area] = $value;
    }

    /**
     * fungsi untuk menampilkan layout
     * @param  string  $view_name nama view sesuai dengan method yang dipanggil
     * @param  string  $template  nama template yang akan dimuat
     * @param  array   $view_data data yang akan ditampilkan pada view
     * @param  string  $name      nama bagian layout yang akan ditempati view
     */
    public function view($view_name, array $view_data, $template = 'backend', $content_area = 'content')
    {
        if ($template == 'backend') {
            $this->set('dashboard_menu', $this->get_dashboard_menu());

            $this->set('list_css', $this->list_css);
            $this->set('list_js', $this->list_js);
            $this->set('modul_custom', $this->get_modul_custom());
        }

        $this->set($content_area, $this->CI->load->view($view_name, $view_data, TRUE));

        $this->CI->load->view('layout/'.$template, $this->template_data);
    }

    /**
     * set dashboard menu
     * @param string $dashboard_menu nama menu modul yang akan dimuat
     */
    public function set_dashboard_menu($dashboard_menu)
    {
        $this->dashboard_menu = $dashboard_menu;
    }

    /**
     * fungsi untuk mendapatkan list menu navigasi untuk halaman dashboard
     * dari file config/navigation.php
     * @return  array  daftar menu navigasi
     */
    public function get_dashboard_menu()
    {
        $this->CI->config->load('navigation');

        return $this->CI->config->item($this->dashboard_menu);
    }

    /**
     * untuk mendapatkan breadcrumbs
     * @param  string $method_alias     nama alias action yang sedang diakses
     * @param  string $controller_alias nama alias controller
     * @return array                    daftar breadcrumb
     */
    public function get_breadcrumbs($method_alias, $controller_alias = '')
    {
        $method     = $this->CI->router->fetch_method();
        $controller = $this->CI->router->fetch_class();
        
        if (empty($controller_alias)) {
            $controller_alias = $controller;
        }

        return array(
            array('title' => 'Beranda', 'url' => base_url()),
            array('title' => ucwords($controller_alias), 'url' => base_url($controller)),
            array('title' => ucwords($method_alias), 'url' => base_url($controller.'/'.$method)),
        );
    }

    /**
     * menambahkan css baru untuk di-load ke layout
     */
    public function add_css($url_css)
    {
        $this->list_css[] = $url_css;
    }

    /**
     * menambahkan js baru untuk di-load ke layout
     * @param [type] $url_js [description]
     */
    public function add_js($url_js)
    {
        $this->list_js[] = $url_js;
    }

    /**
     * mendapatkan list css default
     * @return array list css default layout
     */
    public function init_list_css()
    {
        // ini default css
        $this->list_css = array(
            base_url('assets/vendors/@coreui/icons/css/coreui-icons.min.css'),
            base_url('assets/vendors/flag-icon-css/css/flag-icon.min.css'),
            base_url('assets/vendors/font-awesome/css/font-awesome.min.css'),
            base_url('assets/vendors/simple-line-icons/css/simple-line-icons.css'),
            base_url('assets/css/style.css'),
            base_url('assets/css/app.css'),
            base_url('assets/vendors/pace-progress/css/pace.min.css'),
            base_url('assets/vendors/datatables/datatables.min.css'),
            base_url('assets/vendors/sweetalert2/sweetalert2.min.css'),
            base_url('assets/vendors/select2/select2.min.css'),
            base_url('assets/vendors/select2/select2-bootstrap4.min.css'),
            base_url('assets/vendors/flatpickr/flatpickr.min.css'),
        );
    }

    /**
     * mendapatkan list js default layout
     * @return array list js default layout
     */
    public function init_list_js()
    {
        // init default js
        $this->list_js = array(
            base_url('assets/vendors/jquery/js/jquery.min.js'),
            base_url('assets/vendors/popper.js/js/popper.min.js'),
            base_url('assets/vendors/bootstrap/js/bootstrap.min.js'),
            base_url('assets/vendors/pace-progress/js/pace.min.js'),
            base_url('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js'),
            base_url('assets/vendors/@coreui/coreui/js/coreui.min.js'),
            base_url('assets/vendors/datatables/datatables.min.js'),
            base_url('assets/vendors/sweetalert2/sweetalert2.all.min.js'),
            base_url('assets/vendors/select2/select2.min.js'),
            base_url('assets/vendors/flatpickr/flatpickr.js'),
            base_url('assets/vendors/flatpickr/flatpickr.id.js'),
            base_url('assets/js/app.js'),
            base_url('assets/js/datarange.js'),
        );
    }

    public function get_modul_custom()
    {
        $modul = $this->CI->uri->segment(1);
        
        $icon_color = '#ffffff';

        switch ($modul) {
            case 'direktori_guru':
                $bg_color = 'bg-danger';
                $icon     = 'direktori_guru';
                break;
            
            case 'presensi_online':
                $bg_color = 'bg-warning';
                $icon     = 'moprens';
                break;
            
            case 'surat_menyurat':
                $bg_color = 'bg-primary';
                $icon     = 'mtrasur';
                break;
            
            case 'inventory':
                $bg_color = 'bg-info';
                $icon     = 'avendig';
                break;
            
            default:
                $bg_color = '';
                $icon_color = '#73818f';
                $icon       = 'sistersmazaba';
                break;
        }

        return array(
            'bg_color' => $bg_color,
            'icon_color' => $icon_color,
            'icon_wide'  => $icon . ".png",
            'icon_square'  => $icon . "_square.png",
        );
    }
}
