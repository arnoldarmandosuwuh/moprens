$(document).ready(function(){
    //fungsi untuk mengaktifkan datatable
    $(".table-datatable").DataTable({
        "language": {
            "url": base_url+"assets/vendors/datatables/indonesia.json"
        },
        "columnDefs": [{ "orderable": false, "targets": -1 }]
    });

    //fungsi untuk menampilkan konfirmasi hapus data
    $("body").on("click", ".btn-delete", function(e){
        e.preventDefault();
        var urlhapus = $(this).attr("href");

        Swal.fire({
          title: 'Konfirmasi',
          text: "Apakah Anda yakin akan menghapus data?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f86c6b',
          cancelButtonColor: '#20a8d8',
          confirmButtonText: 'Hapus',
          cancelButtonText: 'Tidak'
        }).then((result) => {
          if (result.value) {
            window.location.href = urlhapus;
          }
        });
    });


    // hide alert setelah 4 detik
    setTimeout(function() {
      $(".alert").hide();
    }, 4000);

    // init select2
    $(".app-select2").select2({
      theme: 'bootstrap4',
    });

    //init datepicker
    $('.app-datetimepicker').flatpickr({
      locale: 'id',
      enableTime: true,
      altInput: true,
      altFormat: "d F Y H:i:S",
      dateFormat: 'Y-m-d H:i:S',
      time_24hr: true,
      enableSeconds: true,
      enableTime: true,
    });


    //init datepicker
    $('.app-datepicker').flatpickr({
      locale: 'id',
      dateFormat: 'Y-m-d',
      altInput: true,
      altFormat: "d F Y",
    });
});