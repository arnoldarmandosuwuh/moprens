$(document).ready(function(){
	state_filter();

	$("#filter").on("change", function(){
		state_filter();
	});

	function state_filter(){
		var filter = $("#filter").val();
		// if (filter == 'bulan') {
		// 	$(".bulan-input").show();
		// 	$(".range-input").hide();
		// 	$(".tujuan-input").hide();
		// }
		if (filter == 'terima') {
			// $(".bulan-input").hide();
			$(".terima-input").show();
			$(".proses-input").hide();
		}
		else if (filter == 'proses') {
			// $(".bulan-input").hide();
			$(".terima-input").hide();
			$(".proses-input").show();
		}
		else if (filter == 'asal') {
			// $(".bulan-input").hide();
			$(".terima-input").hide();
			$(".proses-input").hide();
			$(".asal-input").show();
		}
	}
});
