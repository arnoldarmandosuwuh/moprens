$('.kategori-datatable').DataTable({
    'responsive': true,
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": base_url + 'inventory/master/kategori/get_data_kategori',
        "type": "POST"
    },
    //Set column definition initialisation properties.
    "columns": [
        { "data": "nomor" },
        { "data": "kategori" },
        {
            "data": "pilihan",
            className: 'text-center',
        },
    ],
    "fnCreatedRow": function (row, data, index) {
        var info = $(this).DataTable().page.info();
        var value = index + 1 + info.start;
        $('td', row).eq(0).html(value).attr('align', 'center');
    },
    "columnDefs": [
        {
            "targets": [0, -1], //first column / numbering column
            "orderable": false, //set not orderable
        },
    ],
    "language": {
        "url": base_url + "assets/vendors/datatables/indonesia.json"
    },
});