$('.ruang-periksa-datatable').DataTable({
    'responsive': true,
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [[1, 'asc']], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": base_url + 'inventory/proses/pemeriksaan/get_data',
        "type": "POST"
    },
    //Set column definition initialisation properties.
    "columns": [
        { "data": "nomor" },
        { "data": "ruangan" },
        { "data": "tanggal_cek" },
        {
            "data": "id",
            "render": function (data, type, row, meta) {
                var link = '<div>';
                link += '<a href="' + base_url + 'inventory/proses/pemeriksaan/cek/' + data + '"><i class="fa fa-search"></i> Cek</a>';
                link += '</div>';
                return link;
            },
            className: 'text-center',
        },
    ],
    "fnCreatedRow": function (row, data, index) {
        var info = $(this).DataTable().page.info();
        var value = index + 1 + info.start;
        $('td', row).eq(0).html(value).attr('align', 'center');
    },
    "columnDefs": [
        {
            "targets": [0, -1], //first column / numbering column
            "orderable": false, //set not orderable
        },
    ],
    "language": {
        "url": base_url + "assets/vendors/datatables/indonesia.json"
    },
});