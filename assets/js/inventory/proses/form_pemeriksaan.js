$(function () {
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/proses/pemeriksaan/get_total_barang',
        cache: false,
    });

    var ruang = $("#ruang").val();
    var barang;
    $("#barang").change(function () {
        console.log(ruang);
        barang = $(this).val();
        console.log(barang);

        $.ajax({
            data: { id: ruang, id2: barang },
            success: function (respond) {
                $("#lbl_jumlah_barang_di_ruang").html(respond);
            }
        })
    });
})