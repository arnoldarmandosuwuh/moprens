var ruang = $("#ruang_asal").val();
var barang = $("#barang").val();

ambil_data();
$("#ruang_asal").change(function () {
    ruang = $(this).val();
    $("#lbl_jumlah").html(" ");
    ambil_data();
});

$("#barang").change(function () {
    barang = $(this).val();
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/proses/penempatan/ambil_data',
        cache: false,
    });
    $.ajax({
        data: { modul: 'jumlah', id: ruang, id2: barang },
        success: function (respond) {
            $("#lbl_jumlah").html(respond);
        }
    })
});

$("#jumlah").on('keyup', function () {
    var jumlah = $(this).val();
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/proses/penempatan/is_kelebihan',
        cache: false,
    });
    $.ajax({
        data: { id: ruang, id2: barang, jumlah: jumlah },
        success: function (respond) {
            $("#lbl_jumlah").html(respond);
        }
    });
});

function ambil_data() {
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/proses/penempatan/ambil_data',
        cache: false,
    });
    if (ruang > 0) {
        $.ajax({
            data: { modul: 'barang', id: ruang },
            success: function (respond) {
                $("#barang").html(respond);
            }
        });
        $.ajax({
            data: { modul: 'ruang_tujuan', id: ruang },
            success: function (respond) {
                $("#ruang_tujuan").html(respond);
            }
        })
    }
}