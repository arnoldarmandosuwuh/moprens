// var awal = $('#tanggal_awal').val();
// var akhir = $('#tanggal_akhir').val();
// // $('.no_filter').hide();
// $('.penempatan-datatable').DataTable().destroy();
// // get_notice(awal, akhir);
// fetch_data(awal, akhir);

// $('.filter').click(function () {
//     var awal = $('#tanggal_awal').val();
//     var akhir = $('#tanggal_akhir').val();
//     // $('.no_filter').show();
//     $('.penempatan-datatable').DataTable().destroy();
//     // get_notice(awal, akhir);
//     fetch_data(awal, akhir);
// });

// function fetch_data(awal, akhir) {
$('#penempatan-datatable').DataTable({
    'responsive': true,
    "processing": true, //Feature control the processing indicator.
    // "serverSide": false, //Feature control DataTables' server-side processing mode.
    "order": [[1, "asc"]], //Initial no order.
    // Load data for the table's content from an Ajax source
    // "ajax": {
    //     url: base_url + 'inventory/report/rpt_penempatan/get_data_penempatan?tgl_awal=' + tgl_awal + '&tgl_akhir=' + tgl_akhir,
    //     type: "POST",
    //     // data: {
    //     //     awal: awal, akhir: akhir
    //     // }
    // },
    // //Set column definition initialisation properties.
    // "columns": [
    //     { "data": "nomor" },
    //     { "data": "tanggal_penempatan" },
    //     { "data": "penanggung_jawab" },
    //     { "data": "ruang_asal" },
    //     { "data": "ruang_tujuan" },
    //     { "data": "nama_barang" },
    //     { "data": "jumlah" },
    //     { "data": "user" },
    //     {
    //         "data": "id",
    //         "render": function (data, type, row, meta) {
    //             var link = '<div class="dropdown dropleft">';
    //             link += '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    //             link += '<i class="icon-menu"></i>';
    //             link += '</button>';
    //             link += '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
    //             link += '<a class="dropdown-item" href="' + base_url + 'inventory/report/rpt_penempatan/edit/' + data + '"><i class="fa fa-edit"></i> Edit</a>';
    //             link += '<a class="dropdown-item" target="_blank" href="' + base_url + 'inventory/report/rpt_penempatan/cetak/' + data + '"><i class="fa fa-print"></i> Cetak Detail</a>';
    //             link += '</div>';
    //             link += '</div>';
    //             return link;
    //         },
    //         className: 'text-center',
    //     },
    // ],
    // "fnCreatedRow": function (row, data, index) {
    //     var info = $(this).DataTable().page.info();
    //     var value = index + 1 + info.start;
    //     $('td', row).eq(0).html(value).attr('align', 'center');
    // },
    // "columnDefs": [
    //     {
    //         "targets": [0, -1], //first column / numbering column
    //         "orderable": false, //set not orderable
    //     },
    // ],
    "language": {
        "url": base_url + "assets/vendors/datatables/indonesia.json"
    },
});
// }