get_option_filter(1);
fetch_data_by_ruang(2);
get_notice(1, 2);

$("#jenis_filter").change(function () {
    var jenis_filter = $(this).val();
    get_option_filter(jenis_filter);
});

$('.filter_button').click(function () {
    var id_jenis_filter = $("#jenis_filter").val();
    var id_filter = $("#filter").val();
    if (id_jenis_filter == '' || id_filter == '') {
        alert("PILIH DATA FILTER TERLEBIH DAHULU");
    }
    else {
        if (id_jenis_filter == 1) {
            $('.inventaris-datatable').DataTable().destroy();
            fetch_data_by_ruang(id_filter);
        }
        else {
            $('.inventaris-datatable').DataTable().destroy();
            $('.inventaris-datatable').hide();
            // fetch_data_by_barang(id_filter);
        }
        get_notice(id_jenis_filter, id_filter);
    }
});

function get_notice(id_jenis_filter, id_filter) {
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/report/rpt_inventaris/get_notice_filter',
        cache: false,
    });
    $.ajax({
        data: { id: id_jenis_filter, id2: id_filter },
        success: function (respond) {
            $("#notice").html(respond);
        }
    });
}

function get_option_filter(id_jenis) {
    $.ajaxSetup({
        type: "POST",
        url: base_url + 'inventory/report/rpt_inventaris/get_filter',
        cache: false,
    });
    $.ajax({
        data: { id: id_jenis },
        success: function (respond) {
            $("#filter").html(respond);
        }
    });
}

function fetch_data_by_ruang(id_ruang) {
    $('.inventaris-datatable').DataTable({
        'responsive': true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [[1, "asc"]], //Initial no order.
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: base_url + 'inventory/report/rpt_inventaris/get_data_by_ruang',
            type: "POST",
            data: {
                id: id_ruang
            }
        },
        //Set column definition initialisation properties.
        "columns": [
            { "data": "nomor" },
            { "data": "nama_barang" },
            { "data": "jenis" },
            { "data": "no_inventaris" },
            { "data": "tahun" },
            { "data": "jumlah" },
            { "data": "kondisi_baik" },
            { "data": "kondisi_sedang" },
            { "data": "kondisi_rusak" },
        ],
        "fnCreatedRow": function (row, data, index) {
            var info = $(this).DataTable().page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value).attr('align', 'center');
        },
        "columnDefs": [
            {
                "targets": [0, -1], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
        "language": {
            "url": base_url + "assets/vendors/datatables/indonesia.json"
        },
    });
}