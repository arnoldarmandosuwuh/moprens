$('.surat-datatable').DataTable({
    'responsive': true,
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": base_url + 'surat_menyurat/surat/Surat_masuk/get_data',
        "type": "POST"
    },
    //Set column definition initialisation properties.
    "columns": [
        {"data": "nomor"},
        {"data": "no_agenda"},
        {"data": "asal_surat"},
        {"data": "perihal_surat"},
        {"data": "tgl_surat"},
        {"data": "status_tu"},
        {
          "data": "id",
          "render": function(data, type, row, meta) {
            var link  = '<div class="dropdown dropleft">';
                link +=   '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                link +=     '<i class="icon-menu"></i>';
                link +=   '</button>';
                link +=   '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                link +=     '<a class="dropdown-item" href="'+base_url+'surat_menyurat/surat/Surat_masuk/lengkapi/'+data+'"><i class="fa fa-edit"></i> Lengkapi</a>';
                link +=     '<a class="dropdown-item" href="'+base_url+'surat_menyurat/surat/Surat_masuk/tampil/'+data+'"><i class="fa fa-eye"></i> Tampil</a>';
                link +=     '<a class="dropdown-item" href="'+base_url+'surat_menyurat/surat/Surat_masuk/edit/'+data+'"><i class="fa fa-edit"></i> Edit</a>';
                link +=     '<a class="dropdown-item btn-delete" href="'+base_url+'surat_menyurat/surat/Surat_masuk/hapus/'+data+'"><i class="fa fa-trash"></i> Hapus</a>';
                link +=   '</div>';
                link += '</div>';
            return link;
          },
          className: 'text-center',
        },
    ],
    "fnCreatedRow": function (row, data, index) {
        var info =  $(this).DataTable().page.info();
        var value = index+1+info.start;
        $('td', row).eq(0).html(value).attr('align', 'center');
    },
    "columnDefs": [
        {
            "targets": [ 0 , -1 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
    ],
    "language": {
        "url": base_url+"assets/vendors/datatables/indonesia.json"
    },
});
