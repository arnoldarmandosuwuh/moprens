$('.mata-pelajaran-datatable').DataTable({
    "responsive":true,
    "prossesing":true,
    "serverSide":true,
    "order":[],
    "ajax":{
        "url": base_url+'direktori_guru/master/mata_pelajaran/get_data_mata_pelajaran',
        "type":"POST"
    },
    "columns":
    [
        {"data": "nomor"},
        {"data": "mata_pelajaran"},
        {"data": "keterangan"},
        {
            "data": "id",
            "render":function(data,type,row,meta){
                var link  = '<div class="dropdown dropleft">';
                    link +=   '<button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    link +=     '<i class="icon-menu"></i>';
                    link +=   '</button>';
                    link +=   '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                    link +=     '<a class="dropdown-item" href="'+base_url+'direktori_guru/master/mata_pelajaran/ubah/'+data+'"><i class="fa fa-edit"></i> Ubah</a>';
                    link +=     '<a class="dropdown-item btn-delete" href="'+base_url+'direktori_guru/master/mata_pelajaran/hapus/'+data+'"><i class="fa fa-trash"></i> Hapus</a>';
                    link +=   '</div>';
                    link += '</div>';
                return link;
            },
            className:"text-center",
        },
    ],
    "fnCreatedRow": function(row,data,index){
        var info = $(this).DataTable().page.info();
        var value = index+1+info.start;
        $('td',row).eq(0).html(value).attr('align','center');
    },
    "columnDefs":
    [
        {
            "targets":[0,-1],
            "orderable":false,
        },
    ],
    "language":{
        "url": base_url+"assets/vendors/datatables/indonesia.json"
    },

});

