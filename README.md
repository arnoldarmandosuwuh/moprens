# SISTERSMAZABA
Sistem Informasi Terpadu SMAN 1 Babadan Ponorogo

## CONFIG FILE
buatlah file baru di dalam folder **config** :

- buat file baru dengan nama **config.php** dengan meng-copy dari file **config.example.php** lalu sesuaikan **config.php** dengan pengaturan Anda.
- buat file baru dengan nama **database.php** dengan meng-copy dari file **database.example.php** lalu sesuaikan **database.php** dengan pengaturan Anda.

## IMPORT DATABASE
- buat database baru dengan nama **sistersmazaba** lalu import file database **sistersmazaba.sql** yang ada di folder **db**.
- buat database baru dengan nama **direktori_guru** lalu import file database **direktori_guru.sql** yang ada di folder **db**.


## MENU NAVIGASI
sesuaikan menu navigasi dengan mengedit file **config/navigation.php**

## USERNAME DAN PASSWORD
- username : admin
- password : password